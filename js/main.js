//main js

//required and helpful functions

getBrowserSizes();
$(window).resize(function () {
  getBrowserSizes();
});
function getBrowserSizes() {
  browserWidth = $(window).width();
  browserHeight = $(window).height();
  $("#browsersize").html(browserWidth + "x" + browserHeight);
}

if ($("#footeryear").length > 0) {
  document.getElementById("footeryear").innerHTML = new Date().getFullYear();
}

//form placeholders
$(".contactfields").on("input", function (e) {
  currentinputid = e.currentTarget;
  parentdiv = $(currentinputid).parent();
  //get the label of the parent of input
  var thelabel = parentdiv.find("label");
  countchar = $(currentinputid).val().length;
  if (countchar > 0) {
    thelabel.fadeOut(0);
  } else {
    thelabel.fadeIn(0);
  }
});

$(".input label").on("click", function (e) {
  currentinputid = e.currentTarget;
  $(currentinputid).fadeOut(0);
  $(currentinputid).next("input").focus();
  countchar = $(currentinputid).val().length;
  if (countchar > 0) {
    currentinputid.fadeIn(0);
  }
});
$("input.contactfields").focusout("input", function (e) {
  currentinputid = e.currentTarget;
  parentdiv = $(currentinputid).parent();
  //get the label of the parent of input
  var thelabel = parentdiv.find("label");
  countchar = $(currentinputid).val().length;
  console.log(countchar);
  if (countchar > 0) {
    thelabel.fadeOut(0);
  } else {
    thelabel.fadeIn(0);
  }
});

jQuery(function ($) {
  $("input[name=formdata_phone]").mask("(999) 999-9999");
});

//Basic Swiper Initialization
const swiper = new Swiper(".swiper", {
  // Optional parameters
  direction: "horizontal",
  loop: true,

  // If we need pagination
  // pagination: {
  //   el: '.swiper-pagination',
  // },

  // Navigation arrows
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },

  // And if we need scrollbar
  // scrollbar: {
  //   el: '.swiper-scrollbar',
  // },
});

//smooth scroll
$(function () {
  var offset = -140;
  var scrollTime = 850;

  $('a[href^="#"]').click(function () {
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top + offset,
      },
      scrollTime
    );

    return false;
  });
});

//AJAX SUBMIT FORM
// $('.formsubmit').click(function() {
function toggleSubmitButtonDisability(state) {
  if (state === true) {
    document.getElementById("submitContactForm").disabled = state;
    document.getElementById("submitContactForm").style.display = "none";
  } else {
    document.getElementById("submitContactForm").disabled = false;
    document.getElementById("submitContactForm").style.display = "block";
  }
}
$(document).on("click", ".formsubmit", function () {
  toggleSubmitButtonDisability(true);
  var formid = "#" + $(this).closest("form").prop("id");
  var thanksdiv = $(formid).closest("div").find(".holdthanks");

  //check full name
  if (/\w+\s+\w+/.test($(formid + " input[name=formdata_name]").val())) {
    console.log("good");
  } else {
    console.log("bad");
    $(formid + " .formerror").fadeOut();
    $(formid + " .formerror").html("Please enter a full name.");
    $(formid + " .formerror").fadeIn();
    return false;
  }
  // var moveindate = $('#date_month').val() + '/' + $('#date_day').val() + '/' + $('#date_year').val();
  var url = "action/contact.php";
  // var postData = grabpostdata + "&formdata_name=" + fname + " " + lname;

  var postData = $(formid).serialize();
  // var postData = postData_get + '&formdata_move_in_date=' + moveindate;

  $.ajax({
    type: "POST",
    url: url,
    data: postData,
    dataType: "html",
    beforeSend: function () {
      // setting a timeout
      $(formid + " .formsubmit").addClass("on");
      $(formid + " .formsubmit").html(
        "Sending <span>.</span><span>.</span><span>.</span>"
      );
      console.log(postData);
    },
    success: function (data) {
      $(formid + " .formsubmit").removeClass("on");

      if (data == "Success") {
        $(formid + " .formerror").fadeOut();
        $(formid + ".holdform").fadeOut(function () {
          $(thanksdiv).fadeIn();
        });

        //conversion pixels
      } else if (data == "nameerror") {
        $(formid + " .formerror").html("Please enter a first and last name");
        $(formid + " .formsubmit").html("Contact Us");
        toggleSubmitButtonDisability(false);
      } else {
        $(formid + " .formerror").fadeIn();
        $(formid + " .formerror").html(data);
        $(formid + " .formsubmit").html("Submit");
        toggleSubmitButtonDisability(false);
      }
    },
    // dataType: 'html'
  });
});
//ajax converted to javascript function
async function sendForm() {
  const form = document.querySelector("form");
  const errorElem = document.querySelector(".formerror");
  const holdFormElem = document.querySelector(".holdform");
  const holdThanksElem = document.querySelector(".holdthanks");
  const holdBtnElem = document.querySelector(".holdbtn");
  const holdBtnStatusElem = document.querySelector(".holdbtncontainerstatus");
  holdBtnElem.style.display = "none";
  holdBtnStatusElem.style.display = "block";

  let formDataValues = await new FormData();
  const allFormValues = Object.values(form).reduce((obj, field) => {
    obj[field.name] = field.value;
    formDataValues.append([field.name], field.value);
    return obj;
  }, {});
  const postObj = {
    method: "POST",
    body: formDataValues,
  };
  const url = "action/contact.php";
  const response = await fetch(url, postObj);
  const responseJson = await response.text();
  if (responseJson === "Success") {
    console.log("Success");
    gtag("event", "conversion", {
      send_to: "AW-11111801338/G2AyCNGXrZIYEPrDwrIp",
    });
    holdFormElem.style.display = "none";
    holdThanksElem.style.display = "block";
    holdBtnStatusElem.style.display = "none";
  } else {
    errorElem.innerText = responseJson;
    errorElem.style.display = "block";
    holdBtnElem.style.display = "block";
    holdBtnStatusElem.style.display = "none";
  }
  console.log(responseJson);
}
