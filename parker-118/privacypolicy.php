<?php include('domain.php'); ?>
<?php
//   $domain = "https://nwgclientcontent.com/";
//   $key = "d69cc3f59ec879238e64ab169e034d";
//   $apiResource = "privacypolicy";
//   $pathToPhotos = "admin/storage/uploads";


//   //get filecontents option
//   $arrContextOptions=array(
//       "ssl"=>array(
//           "verify_peer"=>false,
//           "verify_peer_name"=>false,
//       ),
//   );  
//   $apiInformation = file_get_contents($domain . "admin/api/singletons/get/" . $apiResource . "?token=" . $key, false, stream_context_create($arrContextOptions));
//   $privacypolicy = json_decode($apiInformation);
  

//curl option
//   $ch = curl_init();
//   curl_setopt ($ch, CURLOPT_URL,  $domain . "admin/api/singletons/get/" . $apiResource . "?token=" . $key);
//   curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
//   $file_contents = curl_exec($ch);
//   curl_close($ch);
//   $privacypolicy = json_decode($file_contents);

//   var_dump($privacypolicy);
?>
<!DOCTYPE html>
  <html>
    <head>
        <title>Privacy Policy | How We Gather Information</title>
        <meta name="description" content="Your information is valuable to us and we work hard to protect it. Learn how we collect, utilize, and maintain private data from our users.">
        <?php include('header-scripts.php'); ?>  
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
                background-color:#FFFFFF;
            }
            /*fonts*/
            h1 {
                margin-bottom:20px;
            }
            h1,h2 {
                margin-top:20px;
            }
            p {
                margin-top:10px;
            }
            header {
                position: relative;
                width:100%;
                padding:10px 0px;
                margin-bottom:30px;
            }
            header .logo {
                width:150px;
            }
            header .logo img {
                width:100%;
            }
            .pagecontent {
                width:95%;
                margin:0 auto;
                max-width: 800px;
            }
            h1 {
                font-size:22px;
                letter-spacing: -1px;
            }
            h2 {
                font-size:16px;
                letter-spacing: 0px;
            }
            p {
                font-size:12px;
                letter-spacing: 0px;
                line-height:1.4;
            }
            @media only screen and (min-width: 768px) {
               
                header {
                    margin-bottom:40px;
                }
                header .logo {
                    width:200px;
                }
                h1 {
                    font-size:2rem;
                }
                h2 {
                    font-size:1rem;
                }
                p {
                    font-size:12px;
                }
            }

        </style>
    </head>
    <body class="privacypolicy">
      
        <div class="pagecontent">

            <header>
                <div class="logo">
                    <img src="./images/icons/Parker118-Logo-Black.svg" alt="">
                </div>
            </header>

            <h1>Privacy Policy</h1>

            <section>
                <p><span style="font-weight: 400;">Your trust is very important to us and we provide the following to explain our privacy policies with regard to your information. The following online Privacy Policy is intended to protect and secure the personally identifiable information (any information you provide and by which you can be identified) you provide to us online and to ensure you that our commitment and realization of this obligation is to not only meet, but to exceed, most existing privacy standards.&nbsp;&nbsp;</span><span style="font-weight: 400;">&nbsp;</span></p>
                <p><span style="font-weight: 400;">We reserve the right to make changes to this Privacy Policy at any time. If you want to ensure that you are up to date with the latest changes, we suggest that you visit this page frequently. If at any point in time we decide to make use of any personally identifiable information on file in a manner vastly different than that which was stated when this information as initially collected, users will be notified at that time and shall have the option whether to permit the use of their information in this separate manner.</span><span style="font-weight: 400;">&nbsp;</span><br /><br /></p>
                <h2><strong>Information We Collect</strong></h2>
                <p><span style="font-weight: 400;">We collect information from visitors on our website through online requests including: E-newsletters, Contact Requests, Website Forms. The information we collect online includes your name, e-mail address, a phone number (if you want) and specifics about your request, as well as any additional comments you want us to know about you to more specifically frame our communications with you. In addition, we may have occasion to collect non-personal anonymous demographic information such as the type of browser you are using, device, or type of operating system, which will assist us in providing and maintaining superior quality service. In order to continuously improve our service and meet your needs more specifically, we may also ask you to provide information specific to your request as well.</span></p>
                <p><span style="font-weight: 400;">Please note that this Privacy Policy does not govern the collection and use of information and companies that we do not control, nor by individuals not employed nor managed by us. If you visit a website we mention or link to, be sure to review its privacy policy before providing the site with information.<br /><br /></span></p>
                <h2><strong>How the Information You Provide is Used</strong></h2>
                <p><span style="font-weight: 400;">We use your information to communicate with you, to learn more about your needs and to hopefully provide you with a better service experience. We may also use your information to complete a transaction you authorize, to update you on happenings or on upcoming projects or to personalize our Website and its experience for you. We also give you the opportunity to remove your name from our contact lists, if you desire to do so. Please review the corresponding section of this Privacy Policy below. We, through members of our marketing team, may send you e-mails announcing news about our organization, event information and other relevant information related to our company from time to time. Additionally, we&rsquo;ll only send you e-mails that you have agreed to receive by submitting your information on this Website and you can opt out at any time.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Sharing of Personal Information</strong></h2>
                <p><span style="font-weight: 400;">We will never sell, rent, lease or exchange your personal information with other organizations. Accordingly, the identity of all of those who contact us through this Website will be kept confidential. We are also committed to ensuring the security of your personal information. To prevent unauthorized access, maintain data accuracy, and ensure the proper use of your information, we have established and implemented appropriate physical, electronic and managerial protocols to safeguard and secure the information we collect online.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Cookies</strong></h2>
                <p><span style="font-weight: 400;">Based on your submission to us, from time to time, we may send a "cookie" to your computer. A cookie is a piece of data that&rsquo;s sent to your browser from a web server and stored on your computer's hard drive to recognize you when you return to our Website, or to identify which areas of our Website you have visited. A cookie can't read data off your hard drive or read cookie files created by other sites. Cookies also do not damage your system. We may use the information collected to better personalize the content you see on our Website, or to send you more relevant information. You can choose whether to accept cookies by changing the settings of your browser. Your browser can refuse all cookies, or show you when a cookie is being sent. If you choose not to accept these cookies, your experience at our Website and other Websites may be diminished and some features may not work as intended.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Links to Outside Sites</strong></h2>
                <p><span style="font-weight: 400;">This Website links to other websites and also links to documents located on Websites maintained by other organizations or individuals. Once you access a link to another Website, please be aware that we are not responsible for the privacy practices or the content of such Websites and only responsible for the privacy practices while you are engaged on our Website.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Removal from Our Mailing List</strong></h2>
                <p><span style="font-weight: 400;">We give you the opportunity to remove your name from our postal and e-mailing list so that you do not receive any future communications. If you desire to permanently remove your name, you can contact us to do so, or opt out from an email received. If you determine that our information about you is inaccurate or it has changed, you may also modify information by contacting us. If you have comments or questions about our online Privacy Policy, or would like more information about us, or are experiencing technical trouble, please contact us.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Non-Marketing Purposes</strong></h2>
                <p><span style="font-weight: 400;">We greatly respect your privacy. We do maintain and reserve the right to contact you if needed for non-marketing purposes (such as bug alerts, security breaches, account issues, and/or changes in our products and services). In certain circumstances, we may use our website, newspapers, or other public means to post such a notice.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Children Under the Age of 13</strong></h2>
                <p><span style="font-weight: 400;">This website is not directed to, and does not intend knowingly to collect personal identifiable information from children under the age of thirteen (13). If it is determined that such information has been inadvertently collected for any reason, we will take immediate and necessary steps to ensure that such information is deleted from our system&rsquo;s database. Anyone under the age of thirteen (13) must seek and obtain parent or guardian permission before using this Website.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Notice to European Union Users</strong></h2>
                <p><span style="font-weight: 400;">Our operations are primarily based in the United States. If you provide information to us, the information will be transferred out of the European Union (EU) and sent to the United States (the adequacy decision on EU-US Privacy became operational on August 1, 2016. This framework protects the fundamental rights of anyone in the EU whose personal data is transferred to the United States for commercial purposes. It allows the free transfer of data to companies that are certified in the US under the Privacy Shield). By providing personal information to us, you are consenting to its storage and use as described in the Privacy Policy.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Acceptance of Terms</strong></h2>
                <p><span style="font-weight: 400;">By using this website, you are hereby accepting the terms and conditions stipulated within this Privacy Policy agreement. If you are not in agreement with our terms and conditions, then you should refrain from further use of our Websites. In addition, your continued use of our Website following the posting of any updates or changes to our terms and conditions shall mean that you agree and accept such changes.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <h2><strong>Contact Us:</strong></h2>
                <p><span style="font-weight: 400;">Please contact us if you have any questions or comments about this privacy policy or would like to receive more information about us.&nbsp;</span></p>
                <p>&nbsp;</p>     
            </section>


        </div>


      
        <?php include('footer-scripts.php'); ?>
        <script>
            
        </script>
    
    </body>
  </html>