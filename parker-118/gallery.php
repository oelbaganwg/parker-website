<?php include('domain.php'); ?>
<!DOCTYPE html>
<html>

<head>
    <title>The Parker 118 | Photo Gallery | Look Inside</title>
    <meta name="description" content="View a selection of the latest residence, amenity, and neighborhood images for a slice of life at The Parker 118 and imagine yourself here.">
    <link rel="stylesheet" href="css/fancybox.css" />
    <link rel="stylesheet" href="css/aos.css" />
    <?php include('header-scripts.php'); ?>
</head>

<body class="gallery">
    <?php include('menu.php') ?>
    <?php include('_header-inner-page.php') ?>
    <h1 class="innerpage-header">Gallery</h1>

    <!-- gallery-section -->
    <section class="gallery-section">
        <div class="container">
            <div class="tab-container">
                <ul id="filters" class="tab-nav">
                    <li class="tabs-item tabs-item-active all-btn" data-filter="all"><a href="javascript:;" data-id="all">All</a></li>
                    <li class="tabs-item" data-filter="residences"><a href="javascript:;" data-id="residences">Residences</a></li>
                    <li class="tabs-item" data-filter="amenities"><a href="javascript:;" data-id="amenities">Amenities</a></li>
                    <li class="tabs-item" data-filter="neighborhood"><a href="javascript:;" data-id="neighborhood">Neighborhood</a></li>
                    <li class="tabs-item" data-filter="building"><a href="javascript:;" data-id="building">Building</a></li>
                </ul>
                <div class="tab_content">
                    <div class="tab-content-item">
                        <div class="gallery-wrap">
                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496155339-res-header.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496155339-res-header.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496155588-118park-09-edit.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496155588-118park-09-edit.jpg" alt="#"></a>
                            </div>

                            <!-- <div class="gallery-block residences all" style="height:400px">
                                  <a href="./images/new-img-may_5_23/496156626-res-1.jpg" data-fancybox="gallery" ><img src="./images/new-img-may_5_23/496156626-res-1.jpg" alt="#"></a>
                              </div> -->

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496156630-res-2.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496156630-res-2.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496154955-homepage-res.jpg" data-fancybox="gallery">
                                    <img src="./images/new-img-may_5_23/496154955-homepage-res.jpg" alt="#">
                                </a>
                            </div>

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496156633-res-3.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496156633-res-3.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496155795-118park-12.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496155795-118park-12.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496155877-118park-11.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496155877-118park-11.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496156637-res-4.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496156637-res-4.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block residences all" style="height:400px">
                                <a href="./images/new-img-may_5_23/496156642-res-5.jpg" data-fancybox="gallery"><img src="./images/new-img-may_5_23/496156642-res-5.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 31.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 31.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 32.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 32.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 33.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 33.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 34.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 34.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 35.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 35.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 36.jpg" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 36.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 37.jpg
" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 37.jpg
" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 38.jpg
" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 38.jpg
" alt="#"></a>
                            </div>
                            <div class="gallery-block amenities all" style="height:400px">
                                <a href="images/june_22_23/gallery/118Park 39.jpg
" data-fancybox="gallery"><img src="images/june_22_23/gallery/118Park 39.jpg
" alt="#"></a>
                            </div>

                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/gallery/493072726-neigh-4.jpeg" data-fancybox="gallery"><img src="images/gallery/493072726-neigh-4.jpeg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/gallery/Neigh-2.jpg" data-fancybox="gallery"><img src="images/gallery/Neigh-2.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/gallery/Neigh-3.jpg" data-fancybox="gallery"><img src="images/gallery/Neigh-3.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-1.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-1.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-2.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-2.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-3.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-3.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-4.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-4.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-5.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-5.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block neighborhood all" style="height:400px">
                                <a href="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-6.jpg" data-fancybox="gallery"><img src="images/new-img-may_5_23/may_12_23/gallery-neighborhood/gallery-neigh-6.jpg" alt="#"></a>
                            </div>

                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park02sAltB.jpg" data-fancybox="gallery"><img src="images/building/118Park02sAltB.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park20.jpeg" data-fancybox="gallery"><img src="images/building/118Park20.jpeg" alt="#"></a>
                            </div>
                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park19.jpg" data-fancybox="gallery"><img src="images/building/118Park19.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park01AltA.jpg" data-fancybox="gallery"><img src="images/building/118Park01AltA.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park03s.jpg" data-fancybox="gallery"><img src="images/building/118Park03s.jpg" alt="#"></a>
                            </div>
                            <div class="gallery-block building all" style="height:400px">
                                <a href="images/building/118Park18.jpg" data-fancybox="gallery"><img src="images/building/118Park18.jpg" alt="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <?php include('_footer.php') ?>
    <?php include('footer-scripts.php'); ?>
    <script>
        Fancybox.bind("[data-fancybox]", {
            // Your custom options

        });
    </script>

    <script>
        const filterBtns = document.querySelectorAll('.tabs-item');
        const galleryImages = document.querySelectorAll('.gallery-block');

        filterBtns.forEach(btn => {
            btn.addEventListener('click', (e) => {

                // Remove the active class from all li elements
                filterBtns.forEach((item) => {
                    item.classList.remove('tabs-item-active');
                });

                // Add the active class to the clicked li element
                btn.classList.add('tabs-item-active');

                // Get the filter class from the data attribute
                const filterClass = btn.dataset.filter;

                // Loop through all the images
                galleryImages.forEach(img => {
                    // If the image has the filter class, show it, otherwise hide it
                    if (img.classList.contains(filterClass)) {
                        img.style.display = 'block';
                    } else {
                        img.style.display = 'none';
                    }
                });
            });
        });
    </script>
    <script>
        var tl = gsap.timeline();
        tl.to(".header-inner", {
            autoAlpha: 1,
            duration: 1
        }, "<");
        tl.to(".innerpage-header", {
            autoAlpha: 1,
            duration: 1.5
        }, "<");
        tl.from(".innerpage-header", {
            y: 30,
            duration: 1.5
        }, "<");
        tl.to(".gallery-section", {
            autoAlpha: 1,
            duration: 1.5
        }, "<");
        tl.from(".gallery-section", {
            y: 30,
            duration: 1.5
        }, "<");
    </script>
</body>

</html>