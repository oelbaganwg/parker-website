<div class="menu">
  <div class="menu-flex">
    <div class="closex-container">
      <img src="images/icons/closex.svg" alt="">
    </div>

    <!-- <div class="link-container-flex"> -->
      <ul>
        <li>
          <a class="nav-link residences-link" href="./residences">Residences</a>
        </li>
        <li>
          <a class="nav-link amenities-link" href="./amenities">Amenities</a>
        </li>
        <li>
          <a class="nav-link neighborhood-link" href="./neighborhood">Neighborhood</a>
        </li>
        <li>
          <a class="nav-link availability-link" href="./availability">Availability</a>
        </li>
        <li>
          <a class="nav-link gallery-link" href="./gallery">Gallery</a>
        </li>
        <li>
          <a class="nav-link contact-link" href="./contact">Contact</a>
        </li>
      </ul>
      <div class="outside-links-container">
        <a href="./availability">Apply Now</a>
        <a href="https://passport.appf.io/sign_in?idp_type=tportal&vhostless=true" target="_blank">Resident Portal</a>
      </div>
    <!-- </div> -->
  

  <?php include('_footer.php') ?>
  </div>
</div>

<script>
  console.log("Hello from menu");
  const closeX = document.querySelector('.closex-container');
  // const menu = document.querySelector('.menu');

  closeX.addEventListener('click', () => {
    console.log("You clicked the hamburger");

    menu.classList.remove('open');
  })
</script>
<script>
  // Get the current page URL
  var currentUrl = window.location.href;

  // Get all the menu links on the page
  var menuLinks = document.querySelectorAll('.nav-link');

  // Loop through each menu link
  menuLinks.forEach(function(link) {
    // Get the link URL
    var linkUrl = link.getAttribute('href');

    // Check if the link URL matches the current page URL
    if (linkUrl == currentUrl) {
      // Add the "active" class to the link
      link.classList.add('active');
    }
  });

  console.log(currentUrl);
</script>