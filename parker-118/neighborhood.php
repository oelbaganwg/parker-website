<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>The Parker 118 | Our Neighborhood | Rutherford, New Jersey</title>
      <meta name="description" content="Discover local shopping and gourmet dining alongside green parks and abundant transit options in the central, connected community of Rutherford.">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <?php include('header-scripts.php'); ?>  
      
    </head>
    <body class="neighborhood">
      <?php include('menu.php') ?>
      <?php include('_header-inner-page.php') ?>
      <h1 class="innerpage-header">Neighborhood</h1>

      <section class="hero">
          <div class="background-img-container">
            <img src="./images/neighborhood/Neigh_1-Hero.jpeg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy hero-copy-inner">        
            <h2 class="hero-lrg-header">Your Place To<br>Call Home</h2>           
          </div>
        </section>

        <section>
          <div class="container oe-fadeinup">
            <p class="sub-hero-para">Uniting so many incredible ideas in one convenient location, Rutherford offers easy connections to all means of transit from a centrally located commuter suburb alive with its own vibrant local scene. Explore local shops and eateries, soak up the sun in a nearby park, and experience a true sense of community while just moments from the American Dream, The Meadowlands, and so many more exciting locations to enjoy.</p>

        </section>

        <section class="custom-swiper">
  <div class="container oe-fadeinup">
    <div class="swiper-container2">
        <div class="swiper-wrapper">
          <!-- <div class="swiper-slide">
            <img src="images/neighborhood/Mambo.jpg" alt="Mambo Tea House">
          </div>
          <div class="swiper-slide">
            <img src="images/neighborhood/Candlewyck.jpg" alt="Candlewyck Diner">
          </div> -->
          <!-- <div class="swiper-slide">
            <img src="images/neighborhood/Elia.jpg" alt="Elia">
          </div> -->
          <div class="swiper-slide">
            <img src="images/neighborhood/Materas.jpg" alt="Matera's">
          </div>
          <div class="swiper-slide">
            <img src="images/neighborhood/20190712_109ParkAveRutherford-2714.jpeg" alt="Paisano's">
          </div>
          <div class="swiper-slide">
            <img src="images/new-img-may_5_23/may_12_23/neighborhood-dining-suprema.jpg" alt="suprema">
          </div>
          <div class="swiper-slide">
            <img src="images/new-img-may_5_23/may_12_23/neighborhood-dining-fiorentini.jpg" alt="fiorentini">
          </div>
          <div class="swiper-slide">
            <img src="images/new-img-may_5_23/may_12_23/neighborhood-dining-eriebakery.jpg" alt="eriebakery">
          </div>
          <div class="swiper-slide">
            <img src="images/new-img-may_5_23/may_12_23/neighborhood-dining-stevenscafe.jpg" alt="stevenscafe">
          </div>
          <div class="swiper-slide">
            <img src="images/new-img-may_5_23/may_12_23/neighborhood-dining-songenapule.jpg" alt="songenapule">
          </div>
          <!-- <div class="swiper-slide">Slide f</div>
          <div class="swiper-slide">Slide g</div>
          <div class="swiper-slide">Slide h</div>
          <div class="swiper-slide">Slide i</div>
          <div class="swiper-slide">Slide j</div> -->
        </div>
        <!-- Add Pagination -->
        <div class="pagination-container">
          <h3 class="inner-page-section-header oe-fadeinup slider-header">Gourmet<br>Dining</h3>
          <div class="swiper-pagination"></div>
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="flex-container ">
        <div class="flex-img-container tall oe-fadeinup">
          <img src="images/neighborhood/Neigh_2-LocalShopping.jpg" alt="">
        </div>
        <div class="flex-copy-container">
          <h3 class="inner-page-section-header oe-fadeinup">Local<br>Shopping</h3>
          <p class="oe-fadeinup">An eclectic downtown setting provides the perfect backdrop for a pedestrian-friendly shopping scene that combines major chains with unique local outlets. Enjoy all the variety of a major city from a suburban enclave that places endless options right at your doorstep.</p>
        </div>
      </div>
    </div>
  </section>

        <!-- <section> -->
          <div class="container oe-fadeinup">
<!-- Core Swiper Without Styles -->
          <!-- Slider main container -->
            <div class="swiper">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="swip-container">
                  <img src="images/neighborhood/Neigh_Slider-1.jpg" alt="">
                  </div>
                  <p>Metlife Stadium</p>
                </div>
                <div class="swiper-slide">
                  <div class="swip-container">
                  <img src="images/neighborhood/Neigh_Slider-2.jpg" alt="">
                  </div>
                  <p>Sonoma Bistro</p>
                </div>
                <div class="swiper-slide">
                  <div class="swip-container">
                  <img src="images/new-img-may_5_23/may_12_23/Neighborhood-Slider-TrainStation.jpg" alt="">
                  </div>
                  <p>Rutherford Train Station</p>
                </div>
              </div>
              <!-- If we need pagination -->
              <!-- <div class="swiper-pagination"></div> -->

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

              <!-- If we need scrollbar -->
              <!-- <div class="swiper-scrollbar"></div> -->
              
            </div>
          </div>
        <!-- </section> -->
        

        <!-- Map section -->
      <section class="map-section" data-lat="40.82718" data-lng="-74.105462" data-marker="images/118-Map-POI.svg" data-title="Parker">
    <div class="map-container" data-aos="fade">
        <div id="map" class="map"></div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK3vp71vqv_uF_rNu45tCqBwlvEs8HOSQ"></script>
        <script>

            jQuery(document).ready(function($){

                var section_map = $('.map-section');
                var blocks_map_lat = section_map.data('lat') * 1;
                var blocks_map_lng = section_map.data('lng') * 1;
                var blocks_map_title = section_map.data('title');
                var blocks_map_marker = section_map.data('marker');
                var blocks_map = $('.map-block');

                var markers = [];
                var map = [];
                var lat = blocks_map_lat;
                var lng = blocks_map_lng;
                var activeInfoWindow;
                var zoom = 17;

                function initMap() {

                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: lat, lng: lng},
                        zoom: zoom,
                        disableDefaultUI: true,
                        gestureHandling: 'greedy',
                        scrollwheel: false,
                        zoomControl:true,
                        styles: [
                        {
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#f1f1f1"
                            }
                          ]
                        },
                        {
                          "elementType": "labels.icon",
                          "stylers": [
                            {
                              "visibility": "off"
                            }
                          ]
                        },
                        {
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#616161"
                            }
                          ]
                        },
                        {
                          "elementType": "labels.text.stroke",
                          "stylers": [
                            {
                              "color": "#f5f5f5"
                            }
                          ]
                        },
                        {
                          "featureType": "administrative.land_parcel",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#bdbdbd"
                            }
                          ]
                        },
                        {
                          "featureType": "poi",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#eeeeee"
                            }
                          ]
                        },
                        {
                          "featureType": "poi",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#757575"
                            }
                          ]
                        },
                        {
                          "featureType": "poi.park",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#dbdfe1"
                            }
                          ]
                        },
                        {
                          "featureType": "poi.park",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#9e9e9e"
                            }
                          ]
                        },
                        {
                          "featureType": "road",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#ffffff"
                            }
                          ]
                        },
                        {
                          "featureType": "road.arterial",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#757575"
                            }
                          ]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#727374"
                            }
                          ]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#616161"
                            }
                          ]
                        },
                        {
                          "featureType": "road.local",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#9e9e9e"
                            }
                          ]
                        },
                        {
                          "featureType": "transit.line",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#5b5c5d"
                            }
                          ]
                        },
                        {
                          "featureType": "transit.station",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#eeeeee"
                            }
                          ]
                        },
                        {
                          "featureType": "water",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#c9c9c9"
                            }
                          ]
                        },
                        {
                          "featureType": "water",
                          "elementType": "geometry.fill",
                          "stylers": [
                            {
                              "color": "#bbbfc0"
                            }
                          ]
                        },
                        {
                          "featureType": "water",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#9e9e9e"
                            }
                          ]
                        }
                      ]
                    });

                    $.each(blocks_map, function(i, el){

                        var elLiA = $('.showItemOnMap', el);

                        $.each(elLiA, function(index, elLiThis){

                            var marker_lat = $(this).data('lat') * 1;
                            var marker_lng = $(this).data('lng') * 1;

                            $(this).data('marker_index', index);

                            var item = {
                                position: {lat: marker_lat, lng: marker_lng},
                                map: map,
                                type: $(el).data('block'),
                                icon:  {
                                  url: $(el).data('marker'), // url
                                  scaledSize: new google.maps.Size(30, 30)
                                },
                                index: index,
                                message: $(this).data('title'),
                            };

                            var marker = new google.maps.Marker(item);

                            marker.addListener('click', function () {
                                showInfoWindow(marker);
                            });

                            markers.push(marker);
                        });
                    });

                    var item = {
                        position: {lat: blocks_map_lat, lng: blocks_map_lng},
                        map: map,
                        type: 'default',
                        icon: blocks_map_marker,
                        message: blocks_map_title,
                    };

                    var marker = new google.maps.Marker(item);

                    if (blocks_map_title.length > 0) {
                        marker.addListener('click', function () {
                            showInfoWindow(marker);
                        });
                    }

                    markers.push(marker);

                }

                function showInfoWindow(marker) {
                    if (activeInfoWindow) {
                        activeInfoWindow.close();
                    }

                    var infowindow = new google.maps.InfoWindow({content: marker.message});

                    if($(window).width() < 768) {
                    }
                    map.setCenter(marker.getPosition());
                    infowindow.open(map, marker);
                    activeInfoWindow = infowindow;
                }

                function filterMarkers(category) {

                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < markers.length; i++) {
                        marker = markers[i];
                        if (marker.type == category || category.length == 0 || category === false || marker.type == 'default') {
                            marker.setVisible(true);
                            bounds.extend(markers[i].getPosition());
                        } else {
                            marker.setVisible(false);
                        }
                    }

                    if($(window).width() >= 768) {
                        //map.fitBounds(bounds);
                        //map.setCenter(marker.getPosition());
                    }
                }

                $(document).on('click', '.showItemOnMap', function (event) {
                    event.preventDefault();

                    var thCategory = $(this).closest('.map-block').data('block');

                    filterMarkers(thCategory);

                    for (var i = 0; i < markers.length; i++) {
                        marker = markers[i];
                        if (marker.type == thCategory && marker.index == $(this).data('marker_index')) {
                            showInfoWindow(marker);
                        }
                    }

                });

                initMap();
            });

        </script>
    </div>
    <div class="map-info" data-aos="fade">
        <!-- <div class="map-info-head">
            <h3 class="map-title">THE NEIGHBORHOOD</h3>
        </div> -->
        <div class="map-info-body scroll-block">
            <div class="map-block restaurants" data-block="restaurants" data-marker="images/POI-Dining.svg">
                <h3 class="map-block-title open">DINING</h3>
                <ul class="dropdown open">
                  <li><a href="" class="showItemOnMap" data-lat="40.8273011" data-lng="-74.1081721" data-marker_index="" data-title="Song E’ Napule">Song E’ Napule</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8273011" data-lng="-74.1081721" data-marker_index="" data-title="Steven’s Café">Steven’s Café</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="" data-lng="" data-marker_index="" data-title="Salad House (coming soon)">Salad House (coming soon)</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8250243" data-lng="-74.1093455" data-marker_index="" data-title="Cafe Matisse">Cafe Matisse</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8272488" data-lng="-74.1050017" data-marker_index="" data-title="Mambo Tea House">Mambo Tea House</a></li> -->
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8273142" data-lng="-74.1045649" data-marker_index="" data-title="The Risotto House">The Risotto House</a></li> -->
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8273116" data-lng="-74.1038578" data-marker_index="" data-title="Yamada Sushi">Yamada Sushi</a></li> -->
                    <li><a href="" class="showItemOnMap" data-lat="40.829124" data-lng="-74.100154" data-marker_index="" data-title="The New Park Tavern">The New Park Tavern</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8327032" data-lng="-74.0952209" data-marker_index="" data-title="Candlewyck Diner">Candlewyck Diner</a></li> -->
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8281358" data-lng="-74.0973171" data-marker_index="" data-title="Elia">Elia</a></li> -->
                    
                    <li><a href="" class="showItemOnMap" data-lat="40.8281969" data-lng="-74.1027362" data-marker_index="" data-title="Rutherford Pancake House">Rutherford Pancake House</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8276351" data-lng="-74.1038377" data-marker_index="" data-title="Matera's On Park">Matera's On Park</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8280447" data-lng="-74.1013627" data-marker_index="" data-title="Volares">Volares</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8274696" data-lng="-74.1044046" data-marker_index="" data-title="Playa Bowls">Playa Bowls</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.827915" data-lng="-74.1032643" data-marker_index="" data-title="Dunkin'">Dunkin'</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8267182" data-lng="-74.1061401" data-marker_index="" data-title="Paisano's">Paisano's</a></li> -->
                    <li><a href="" class="showItemOnMap" data-lat="40.8285672" data-lng="-74.1018557" data-marker_index="" data-title="Ara Coffee">Ara Coffee</a></li>

                    <li><a href="" class="showItemOnMap" data-lat="40.82729833054388" data-lng="-74.10470094642778" data-marker_index="" data-title="Suprema">Suprema</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.827354464422385" data-lng="-74.10490831554792" data-marker_index="" data-title="Fiorentini">Fiorentini</a></li>
                    
                    <li><a href="" class="showItemOnMap" data-lat="40.828083690310244" data-lng="-74.10395318875584" data-marker_index="" data-title="Erie Coffeeshop & Bakery">Erie Coffeeshop & Bakery</a></li>
                </ul>
            </div>

          <div class="map-block school" data-block="school" data-marker="images/POI-Schools.svg">
                <h3 class="map-block-title">SCHOOLS</h3>
                <ul class="dropdown">
                    <li><a href="" class="showItemOnMap" data-lat="40.8331009" data-lng="-74.106607" data-marker_index="" data-title="Washington School">Washington School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8290208" data-lng="-74.1082134" data-marker_index="" data-title="Rutherford High School">Rutherford High School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.821247" data-lng="-74.1111338" data-marker_index="" data-title="Pierrepont School">Pierrepont School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8260379" data-lng="-74.108424" data-marker_index="" data-title="Rutherford Public Schools">Rutherford Public Schools</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8240994" data-lng="-74.1058587" data-marker_index="" data-title="Kindergarten Center">Kindergarten Center</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.82664" data-lng="-74.10735" data-marker_index="" data-title="Goldilocks Children Learning Center">Schoolhouse Early Learning LLC</a></li>

                </ul>
            </div>

            <div class="map-block lifestyle" data-block="lifestyle" data-marker="images/POI-Lifestyle.svg">
                <h3 class="map-block-title">LIFESTYLE</h3>
                <ul class="dropdown">
                     <li><a href="" class="showItemOnMap" data-lat="40.8262763" data-lng="-74.1077909" data-marker_index="" data-title="United States Postal Service">United States Postal Service</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.823177" data-lng="-74.099625" data-marker_index="" data-title="Club Metro USA">Club Metro USA</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.829938" data-lng="-74.0994319" data-marker_index="" data-title="Dog House Bakery & Grooming Salon">Dog House Bakery & Grooming Salon</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8279532" data-lng="-74.1031747" data-marker_index="" data-title="Substance Salon & Barberspa">Substance Salon & Barberspa</a></li>

                     <li><a href="" class="showItemOnMap" data-lat="40.8280996" data-lng="-74.0997138" data-marker_index="" data-title="Strong & Shapely Gym">Strong & Shapely Gym</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8279655" data-lng="-74.1017663" data-marker_index="" data-title="Personal Touch Fitness">Personal Touch Fitness</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8277164" data-lng="-74.0998621" data-marker_index="" data-title="Untamed Boxing And Fitness">Untamed Boxing And Fitness</a></li>
                </ul>
            </div>

            <div class="map-block entertainment" data-block="entertainment" data-marker="images/POI-Entertainment.svg">
                <h3 class="map-block-title">ENTERTAINMENT</h3>
                <ul class="dropdown">
                    <li><a href="" class="showItemOnMap" data-lat="40.814639" data-lng="-74.1040694" data-marker_index="" data-title="Meadowlands Museum">Meadowlands Museum</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.82708" data-lng="-74.102933" data-marker_index="" data-title="Center Cinemas">Center Cinemas</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8277975" data-lng="-74.1040201" data-marker_index="" data-title="Paint Zone Art Studio">Paint Zone Art Studio</a></li>
                </ul>
            </div>

            <div class="map-block shop" data-block="shop" data-marker="images/POI-Shopping.svg">
                <h3 class="map-block-title">SHOPPING</h3>
                <ul class="dropdown">
                    <li><a href="" class="showItemOnMap" data-lat="40.8243284" data-lng="-74.1124286" data-marker_index="" data-title="Rutherford Wine Shoppe">Rutherford Wine Shoppe</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8270426" data-lng="-74.1045279" data-marker_index="" data-title="Rutherford Florist">Rutherford Florist</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8278126" data-lng="-74.1034181" data-marker_index="" data-title="Varrelmann's Bake Shop">Varrelmann's Bake Shop</a></li>

                    <li><a href="" class="showItemOnMap" data-lat="40.8311036" data-lng="-74.0932545" data-marker_index="" data-title="Liberty Commons LLC">Liberty Commons LLC</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8308289" data-lng="-74.0898786" data-marker_index="" data-title="Rutherford Commons">Rutherford Commons</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8234437" data-lng="-74.1140313" data-marker_index="" data-title="Krauszer's Food and Liquor Store">Krauszer's Food and Liquor Store</a></li>
                </ul>
            </div>

            <div class="map-block park" data-block="park" data-marker="images/POI-Parks.svg">
                <h3 class="map-block-title">PARK</h3>
                <ul class="dropdown">
                    <li><a href="" class="showItemOnMap" data-lat="40.8381911" data-lng="-74.1156216" data-marker_index="" data-title="Sunset Park">Sunset Park</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8259796" data-lng="-74.0979349" data-marker_index="" data-title="Riggin Memorial Field">Riggin Memorial Field</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8216893" data-lng="-74.1014221" data-marker_index="" data-title="Wall Field">Wall Field</a></li>
                </ul>
            </div>

            <div class="map-block transit" data-block="transit" data-marker="images/POI-Transit.svg">
                <h3 class="map-block-title">TRANSIT</h3>
                <ul class="dropdown">
                    <li><a href="" class="showItemOnMap" data-lat="40.828277" data-lng="-74.1008057" data-marker_index="" data-title="Rutherford Train Station">Rutherford Train Station</a></li>
                </ul>
            </div>
            <!-- <br>
            <br> -->
        </div>
    </div>
</section>


  <?php include('_footer.php') ?>

  <!-- Swiper JS -->
  <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

  <!-- Initialize Swiper -->
  <script>
    // let labels = ["Mambo Tea House", "Candlewyck Diner", "Matera's On Park", "Paisano's"];
    let labels = ["Matera's On Park", "Matisse", "Suprema", "Fiorentini", "Erie Coffeshop & Bakery", "Steven's Cafe", "Song E Napule"];
var swiperTest = new Swiper('.swiper-container2', {
  slidesPerView: 1,
  effect: "fade",
  disableDraggable: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
    renderBullet: function(index, className) {
      return '<div class="' + className + '">' + (labels[index]) +
        '</div>';

    },
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  //   nextEl: ".next",
  //   prevEl: ".prev",
  },
  loop: true
});

  </script>
    

      
      <?php include('footer-scripts.php'); ?>
      <script>
      
      const dropdowns = document.querySelectorAll('.dropdown');
      const mapBlockTitles = document.querySelectorAll('.map-block-title');

    

    // Loop through each mapBlockTitle element and add a click event listener
    mapBlockTitles.forEach((mapBlockTitle) => {
      mapBlockTitle.addEventListener('click', (e) => {

      mapBlockTitles.forEach(title => {
        if (title !== mapBlockTitle) {
          title.classList.remove('open');
        }
      });

      mapBlockTitle.classList.add('open');
        
    
    // Remove the 'open' class from all sibling elements
    dropdowns.forEach(dropdown => {
      dropdown.classList.remove('open');
    });
    
    // Add the 'open' class to the sibling element of the clicked mapBlockTitle
    const dropDownToOpen = mapBlockTitle.nextElementSibling;
    dropDownToOpen.classList.add('open');


  });
});

    
      </script>

      <script>
        var tl = gsap.timeline();
        tl.to(".hero", {autoAlpha: 1, duration: 1});
        tl.to(".header-inner", {autoAlpha: 1, duration: 1}, "<");
        tl.to(".innerpage-header", {autoAlpha: 1, duration: 1.5}, "<");
        tl.from(".innerpage-header", {y: 30, duration: 1.5}, "<");

      </script>
    </body>
  </html>