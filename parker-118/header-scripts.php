      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
      <link rel="icon" href="<?php echo $domain; ?>images/Parker118-Favicon.jpg">
      <link rel="stylesheet" href="<?php echo $domain; ?>styles/styles.min.css?v=1631739744037">
      <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '476540372913094');
        fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=476540372913094&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-PHEVC1073G"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-PV4704TF9S"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=AW-11111801338"></script>

      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-PHEVC1073G'); //master
        gtag('config', 'G-PV4704TF9S'); //parker 118
        gtag('config', 'AW-11111801338', {'allow_enhanced_conversions':true}); //parker 118 Google Ads
        // gtag('config', 'AW-1029912699');
        // gtag('event', 'page_view', {
        //   'send_to': 'AW-1029912699'
        // });
      </script>
      