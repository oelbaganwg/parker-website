<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>The Parker 118 | Explore Residences | Fixtures & Finishes</title>
      <meta name="description" content="Explore kitchen, living, bed, and bathroom features. Comfort and style combine in a selection of spacious layouts made for modern living.">
      <?php include('header-scripts.php'); ?>  
    </head>
    <body class="residences">
      <?php include('menu.php') ?>
      <?php include('_header-inner-page.php') ?>
      <h1 class="innerpage-header">Residences</h1>

      <section class="hero">
          <div class="background-img-container">
            <!-- <img src="./images/residences/Res_1-Hero.jpg" alt=""> -->
            <img src="./images/new-img-may_5_23/496155339-res-header.jpg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy hero-copy-inner">        
            <h2 class="hero-lrg-header">As Good<br>As It Gets</h2>           
          </div>
        </section>

        <section>
          <div class="container">
            <p class="sub-hero-para oe-fadeinup">Comfort and style combine in a selection of spacious layouts made for modern living. Host your next dinner party in a chef-inspired open kitchen. Unwind in your spa-inspired bathroom. Appreciate the finer things like quartz countertops and the convenience of in-home laundry in the collection that carefully considers your every need.</p>

            <!-- Core Swiper Without Styles -->
          <!-- Slider main container -->
            <div class="swiper oe-fadeinup">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                  <!-- <img src="images/residences/Res_Slider-1.jpg" alt=""> -->
                  <img src="./images/new-img-may_5_23/496155588-118park-09-edit.jpg" alt="">
                  <p>Light-Filled Living Spaces</p>
                </div>

                <div class="swiper-slide">
                  <!-- <img src="images/residences/Res_Slider-2.jpg" alt=""> -->
                  <img src="./images/new-img-may_5_23/496155795-118park-12.jpg" alt="">
                  <p>Chef-Inspired Kitchen Design</p>
                </div>

                <div class="swiper-slide">
                  <!-- <img src="images/residences/Res_Slider-3.jpg" alt=""> -->
                  <img src="./images/new-img-may_5_23/496155877-118park-11.jpg" alt="">
                  <p>Spa-Like Bathroom Escapes</p>
                </div>
              </div>
              <!-- If we need pagination -->
              <!-- <div class="swiper-pagination"></div> -->

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

              <!-- If we need scrollbar -->
              <!-- <div class="swiper-scrollbar"></div> -->
              <!-- <p>Light-Filled Living Spaces</p> -->
            </div>
          </div>
        </section>

        <section class="living-and-dining  oe-fadeinup">
          <div class="container flex-container">
            <div class="flex-copy-container">
              <h3 class="inner-page-section-header">Living<br>& Dining</h3>
              <ul>
                <li>Full Size In-Home Washer and Dryer</li>
                <li>Luxury Plank Flooring</li>
                <li>Energy-Efficient Central Heat and Air</li>
                <li>Oversized Operable Windows with Window Treatments</li>
                <li>High Ceilings</li>
                <li>Spacious Custom Closets</li>
                <li>Pre-Wired for FIOS</li>
              </ul>
            </div>
            <div class="flex-img-container  oe-fadeinup">
              <!-- <img src="images/residences/Res-Living-1.jpg" alt=""> -->
              <img src="./images/new-img-may_5_23/496156626-res-1.jpg" alt="">
            </div>
          </div>
        </section>

        <section class="living-section-grid">
          <div class="container grid">
            <div class="grid-child grid-child-1">
              <!-- <img src="images/residences/Res-Living-2.jpg" alt="" class="oe-fadeinup"> -->
              <img src="./images/new-img-may_5_23/496156630-res-2.jpg" alt="">
              <p class="oe-fadeinup">Gourmet Kitchens Featuring Modern Gray Soft Close Cabinetry, Quartz Countertops, Kohler Fixtures and Stainless Steel Appliances.</p>
            </div>
            <div class="grid-child grid-child-2 oe-fadeinup">
              <!-- <img src="images/residences/Res-Living-3.jpg" alt=""> -->
              <img src="./images/new-img-may_5_23/496156633-res-3.jpg" alt="">              
            </div>
          </div>
        </section>

        <section class="large-letters-grey-background">
          <div class="container">
            <h4 class="lrg-header oe-fadeinup">
            A Premier Lifestyle Experience, Defined By Hand-selected Fixtures & Finishes
            </h4>
          </div>
        </section>

        <section class="bath-section-grid">
          <div class="container grid">
            <div class="grid-child grid-child-1 oe-fadeinup">
              <!-- <img src="images/residences/Res-Bath-1.jpg" alt=""> -->
              <img src="./images/new-img-may_5_23/496156637-res-4.jpg" alt="">               
            </div>
            <div class="grid-child-2">
              <h3 class="inner-page-section-header oe-fadeinup">Baths</h3>
              <ul>
                <li class="oe-fadeinup">Spa-like Master Baths with Custom Vanities</li>
                <li class="oe-fadeinup">and Quartz Countertops</li>
              </ul>
            </div>
            <div class="grid-child grid-child-3 oe-fadeinup">
              <!-- <img src="images/residences/Res-Bath-2.jpg" alt=""> -->
              <img src="./images/new-img-may_5_23/496156642-res-5.jpg" alt="">                 
            </div>
          </div>
        </section>

      

      <?php include('_footer.php') ?>
      <?php include('footer-scripts.php'); ?>
      <script>
        
      </script>
      <script>
    var tl = gsap.timeline();
    tl.to(".hero", {autoAlpha: 1, duration: 1});
    tl.to(".header-inner", {autoAlpha: 1, duration: 1});
    tl.to(".innerpage-header", {autoAlpha: 1, duration: 1.5}, "<");
    tl.from(".innerpage-header", {y: 30, duration: 1.5}, "<");

    </script>
    </body>
  </html>