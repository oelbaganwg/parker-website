<header class="header header-inner">
  <div class="incentive">
  Ask Our Leasing Team About Current Move&#8209;In Specials!
  </div>
  <nav class="nav">
    <div class="nav-flex container">
      <div class="communities-dropdown">Communities
        <div class="dropdown-link-container innerpage-dropdown">
          <ul>
            <li><a href="../parker-106/">The Parker 106</a></li>
            <li class="active"><a href="./">The Parker 118</a></li>
          </ul>
        </div>  
        <!-- <div class="transparent-background"></div> -->
      </div>
      <div class="header-logo-container">
        <a href="./">
          <img src="images/icons/Parker118-Logo-Black.svg" alt="">
        </a>
      </div>
      <div class="header-links-container">
        <a class="header-link" href="./availability">Availability</a>
        <a class="header-link" href="./contact">Contact</a>
        <!-- <a class="header-link desktopxllink" href="./availability">Apply Now</a> -->
        <div class="hamburger-container hamburger-inner">
          <img src="images/icons/Menu-black.svg" alt="">
        </div>
      </div>
    </div>
    
  </nav>
  <div class="transparent-background"></div>
</header>


<script>
  console.log("Hello from inner page header");
  const hamburger = document.querySelector('.hamburger-inner');
  const menu = document.querySelector('.menu');
  const communitiesDropdown = document.querySelector('.communities-dropdown');


  hamburger.addEventListener('click', () => {
    console.log("You clicked the hamburger");

    menu.classList.add('open');
  });

  communitiesDropdown.addEventListener('click', () => {
    console.log("You clicked the Communities dropdown");

    communitiesDropdown.classList.toggle('open');

    // menu.classList.add('open');
  });
</script>