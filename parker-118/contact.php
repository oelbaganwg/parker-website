<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>The Parker 118 | Contact Us | Schedule a Tour</title>
      <meta name="description" content="Let us know a little more about you and join our VIP update list today. Speak to a member of our leasing team to schedule a private visit.">
      <link rel="stylesheet" href="css/fancybox.css"/>
      <link rel="stylesheet" href="css/aos.css"/>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <?php include('header-scripts.php'); ?>  
    </head>
    <body class="contact">
      <?php include('menu.php') ?>

      <div class="contact-page-flex">
      <?php include('_header-inner-page.php') ?>
      
      <?php include('form.php') ?>
      

        
    

      <?php include('_footer.php') ?>
      </div>

      <?php include('footer-scripts.php'); ?>
      
      <script>
      const calendarfield = document.getElementById("calendarfield");
      const calendarinput = document.getElementById("calendarinput");
      function showCalendarInput() {
          calendarinput.style.opacity = 1
      }
    </script>
    <script>
        var tl = gsap.timeline();
        
        tl.to(".header-inner", {autoAlpha: 1, duration: 1}, "<");
        tl.to(".contact-lrg-header", {autoAlpha: 1, duration: 1.5}, "<");
        tl.from(".contact-lrg-header", {y: 30, duration: 1.5}, "<");
        tl.to(".formarea", {autoAlpha: 1, duration: 1.5}, "<");
        tl.from(".formarea", {y: 30, duration: 1.5}, "<");

      </script>
    </body>
  </html>