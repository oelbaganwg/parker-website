<?php include('domain.php'); ?>
<!DOCTYPE html>
<html>

<head>
  <title>The Parker 118 | Current Availability | Explore Floor Plans</title>
  <meta name="description" content="View current pricing and availability for our new luxury residences and find the spacious studio, one, or two bedroom home of your dreams.">
  <link rel="stylesheet" href="css/fancybox.css" />
  <link rel="stylesheet" href="css/aos.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <?php include('header-scripts.php'); ?>
  <style>
    .availability .location-button-container {
      margin-bottom: 5rem;
      text-align: center;
  }

  .availability .location-button-container .location-btn {
    color: #000;
    text-transform: uppercase;
    border: 1px solid #000;
    padding: .5em 1em;
    border-radius: 50px;
    transition: all .5s ease;
    width: 150px;
    
  }

  .availability .location-button-container .location-btn:first-child {
    margin-right: 1rem;
  }
  
  .availability .location-button-container .active,
  .availability .location-button-container .location-btn:hover
   {
    background-color: black;
    color: white;
  }
  
  
  </style>
</head>

<body class="availability">
  <?php include('menu.php') ?>
  <?php include('_header-inner-page.php') ?>
  <h1 class="innerpage-header">Availability</h1>
  <!-- <h3 class="innerpage-subheader">*Pricing reflects net effective rents based on 1 month free on a 13-month lease.</h3> -->
  <!-- gallery-section
      <section class="gallery-section">
          <div class="container">
              <div class="tab-container">
                  <ul id="filters" class="tab-nav" data-aos="fade-up">
                      <li class="tabs-item tabs-item-active"><a href="#" data-id="all" data-filter="*">All</a></li>
                      <li class="tabs-item"><a href="#" data-id="residences" data-filter=".residences">Residences</a></li>
                      <li class="tabs-item"><a href="#" data-id="amenities" data-filter=".amenities">Amenities</a></li>
                      <li class="tabs-item"><a href="#" data-id="neighborhood" data-filter=".neighborhood">Neighborhood</a></li>
                  </ul>
                  <div class="tab_content">
                      <div class="tab-content-item">
                          <div class="gallery-wrap">

                              <div class="gallery-block residences">
                                  <a href="images/gallery/Res-1.jpg" data-fancybox="gallery" data-aos="fade-up">
                                    <img src="images/gallery/Res-1.jpg" alt="#">
                                  </a>
                              </div>
                              <div class="gallery-block residences">
                                  <a href="images/gallery/Res-2.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Res-2.jpg" alt="#"></a>
                              </div>
                              <div class="gallery-block residences">
                                  <a href="images/gallery/Res-3.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Res-3.jpg" alt="#"></a>
                              </div>
                              
                              <div class="gallery-block amenities">
                                  <a href="images/gallery/Amen-1.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Amen-1.jpg" alt="#"></a>
                              </div>
                              <div class="gallery-block amenities">
                                  <a href="images/gallery/Amen-2.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Amen-2.jpg" alt="#"></a>
                              </div>
                              <div class="gallery-block amenities">
                                  <a href="images/gallery/Amen-3.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Amen-3.jpg" alt="#"></a>
                              </div>
                            
                              <div class="gallery-block neighborhood">
                                  <a href="images/gallery/Neigh-1.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Neigh-1.jpg" alt="#"></a>
                              </div>
                              <div class="gallery-block neighborhood">
                                  <a href="images/gallery/Neigh-2.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Neigh-2.jpg" alt="#"></a>
                              </div>
                              <div class="gallery-block neighborhood">
                                  <a href="images/gallery/Neigh-3.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery/Neigh-3.jpg" alt="#"></a>
                              </div>
                              
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section> -->
      
  <section>
    <script type='text/javascript' charset='utf-8'>
      document.write(unescape("%3Cscript src='" + (('https:' == document.location.protocol) ? 'https:' : 'http:') + "//vangorealestate.appfolio.com/javascripts/listing.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <div class="location-button-container">
      <button id="button118" class="active location-btn">Parker 118</button>
      <button id="button106" class="location-btn">Parker 106</button>
      </div>

    <div id="118Listings" style="display: block;">
     
    <script type='text/javascript' charset='utf-8'>
      Appfolio.Listing({
        hostUrl: 'vangorealestate.appfolio.com',
        propertyGroup: '118 Park Urban Renewal LLC',
        themeColor: '#6E6E6E',
        height: '500px',
        width: '100%',
        defaultOrder: 'date_posted'
      });
    </script>
    </div>

<div id="106Listings" style="display: none;">
  
    <script type='text/javascript' charset='utf-8'>
  Appfolio.Listing({
    hostUrl: 'vangorealestate.appfolio.com',
    propertyGroup: 'Park Ave Rutherford Urban Renewal Company LLC',
    themeColor: '#6E6E6E',
    height: '500px',
    width: '100%',
    defaultOrder: 'date_posted'
  });
</script>
</div>
  </section>

  <script>
    const button118 = document.getElementById('button118');
    const button106 = document.getElementById('button106');
    const listings118 = document.getElementById('118Listings');
    const listings106 = document.getElementById('106Listings');


    document.addEventListener('DOMContentLoaded', function() {
      button118.addEventListener('click', function() {
        
        listings118.style.display = 'block';
        listings106.style.display = 'none';
        button118.classList.add('active');
        button106.classList.remove('active');
        
      });

      button106.addEventListener('click', function() {
        
        listings118.style.display = 'none';
        listings106.style.display = 'block';
        button118.classList.remove('active');
        button106.classList.add('active');
        
        
      });
    });
  </script>




  <?php include('_footer.php') ?>
  <?php include('footer-scripts.php'); ?>

  <script>
    var tl = gsap.timeline();
    tl.to(".header-inner", {
      autoAlpha: 1,
      duration: 1
    }, "<");
    tl.to(".innerpage-header", {
      autoAlpha: 1,
      duration: 1.5
    }, "<");
    tl.from(".innerpage-header", {
      y: 30,
      duration: 1.5
    }, "<");
  </script>
</body>

</html>