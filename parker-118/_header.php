<header class="header">
  <div class="incentive">
  Ask Our Leasing Team About Current Move&#8209;In Specials!
  </div>
  <nav class="nav">
    <div class="nav-flex container">
      <div class="communities-dropdown">Communities
        <div class="dropdown-link-container">
          <ul>
            <li><a href="../parker-106/">Parker 106</a></li>
            <li class="active"><a href="./index">Parker 118</a></li>
          </ul>
        </div>  
      </div>
      <div class="header-logo-container">
        <a href="./">
          <img class="parker-logo" src="images/icons/Parker118_logo-white.svg" alt="">
        </a>
      </div>
      <div class="header-links-container">
        <a class="header-link" href="./availability">Availability</a>
        <a class="header-link" href="./contact">Contact</a>
        <!-- <a class="header-link desktopxllink" href="./availability">Apply Now</a> -->
        <div class="hamburger-container">
          <img class="hamburger" src="images/icons/Menu-white.svg" alt="">
        </div>
      </div>
    </div>
  </nav>
</header>

<script>
  
  const hamburger = document.querySelector('.hamburger-container');
  const menu = document.querySelector('.menu');
  const communitiesDropdown = document.querySelector('.communities-dropdown');

  hamburger.addEventListener('click', () => {
    console.log("You clicked the hamburger");

    menu.classList.add('open');
  });

  communitiesDropdown.addEventListener('click', () => {
    console.log("You clicked the Communities dropdown");

    communitiesDropdown.classList.toggle('open');

    // menu.classList.add('open');
  });
</script>

<script>
const parkerLogo = document.querySelector('.parker-logo');
const parkerLogoBlack = 'images/icons/Parker118-Logo-Black.svg';
const parkerLogoWhite = 'images/icons/Parker118_logo-white.svg';
const hamburgerImg = document.querySelector('.hamburger');
const hamburgerWhite = 'images/icons/Menu-white.svg';
const hamburgerBlack = 'images/icons/Menu-black.svg';
const header = document.querySelector('.header');
const scrollThreshold = 50; // number of pixels to trigger style change

window.addEventListener('scroll', () => {
  if (window.pageYOffset > scrollThreshold) {
    header.classList.add('scrolled');
    parkerLogo.src = parkerLogoBlack;
    hamburgerImg.src = hamburgerBlack;
  } else {
    header.classList.remove('scrolled');
    parkerLogo.src = parkerLogoWhite;
    hamburgerImg.src = hamburgerWhite;
  }
});
</script>