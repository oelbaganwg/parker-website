<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>The Parker 118 | New Luxury Rentals | Studio, 1 & 2 Bedroom</title>
      <meta name="description" content="Park Avenue living with residences made for the modern world and amenities that offer a lifestyle of luxury in a central, connected neighborhood.">
      <link rel="icon" type="image/png" href="images/favicon.png">
      <!-- <link rel="stylesheet" href="css/fullpage.css"/>
      <link rel="stylesheet" href="css/fancybox.css"/>
      <link rel="stylesheet" href="css/aos.css"/>
      <link rel="stylesheet" href="css/style.css"/> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <?php include('header-scripts.php'); ?>  
    </head>
    <body>

      <?php include('menu.php') ?>
      <?php include('_header.php') ?>

      <main>
        <!-- main hero -->
        <section class="hero main-hero">
          <div class="background-img-container">
            <!-- <img src="./images/home/Home_1-Hero.jpeg" alt=""> -->
            <img src="./images/new-img-may_5_23/496154920-homepage-header.jpg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy">
            <p class="hero-sml-header">New Studio, One & Two Bedroom Luxury&nbsp;Rentals</p>
            <h1 class="hero-lrg-header">Park Avenue Living</h1>
            <a href="./availability">
              <button class="btn">Apply Now</button>
            </a>
          </div>
        </section>

        <!-- Residences hero -->

        <section class="hero">
          <div class="background-img-container">
            <!-- <img src="./images/home/Home_2-Res.jpg" alt=""> -->
          <img src="./images/new-img-may_5_23/496154955-homepage-res.jpg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy">
            <p class="hero-sml-header oe-fadeinup">Made For The Modern World</p>
            <h2 class="hero-lrg-header oe-fadeinup">Residences</h2>
            <a href="./residences">
              <button class="btn">Explore</button>
            </a>
          </div>
        </section>

        <!-- Amenities hero -->

        <section class="hero">
          <div class="background-img-container">
            <!-- <img src="./images/home/Home_3-Amenities.jpg" alt=""> -->
            <img src="./images/new-img-may_5_23/may_12_23/home_amenity.jpg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy">
            <p class="hero-sml-header oe-fadeinup">A New Lifestyle Of Luxury</p>
            <h2 class="hero-lrg-header oe-fadeinup">Amenities</h2>
            <a href="./amenities">
              <button class="btn">Explore</button>
            </a>
          </div>
        </section>

        <!-- Neighborhood hero -->

        <section class="hero">
          <div class="background-img-container">
            <img src="./images/home/493070583-home_4-neighborhood3.jpeg" alt="">
          </div>
          <div class="gradient"></div>
          <div class="container hero-copy">
            <p class="hero-sml-header oe-fadeinup">A Central, Connected Neighborhood</p>
            <h2 class="hero-lrg-header oe-fadeinup">Rutherford</h2>
            <a href="./neighborhood">
              <button class="btn">Explore</button>
            </a>
          </div>
        </section>
      
        <?php include('form.php') ?>
      
      
    <!-- check <a href="./instructions.php">instructions</a> -->

    <!-- <a href="./residences">Residences</a> -->
    

      </main>
      <?php include('_footer.php') ?>
      
      <?php include('footer-scripts.php'); ?>
      <script>
    const calendarfield = document.getElementById("calendarfield");
    const calendarinput = document.getElementById("calendarinput");
    function showCalendarInput() {
        calendarinput.style.opacity = 1
    }
</script>

  <script>
    var tl = gsap.timeline();
    tl.to(".hero", {autoAlpha: 1, duration: 1});
    tl.to(".header", {autoAlpha: 1, duration: 1});
    tl.to(".hero-copy", {autoAlpha: 1, duration: 1.5}, "<");
    tl.from(".hero-copy", {y: 30, duration: 1.5}, "<");


   
  </script>

    </body>
  </html>

  <!-- Test Comment -->