<?php include('domain.php'); ?>
<!DOCTYPE html>
<html>

<head>
  <title>The Parker 118 | Featured Amenities | Indoor & Outdoor</title>
  <meta name="description" content="Our carefully curated amenities offer support and blissful transitions throughout your day. Explore a fitness center, landscaped courtyard, and more.">
  <?php include('header-scripts.php'); ?>
</head>

<body class="amenities">
  <?php include('menu.php') ?>
  <?php include('_header-inner-page.php') ?>
  <h1 class="innerpage-header">Amenities</h1>

  <section class="hero">
    <div class="background-img-container">
      <!-- <img src="./images/amenities/Amen_1-Hero.jpeg" alt=""> -->
      <img src="./images/new-img-may_5_23/may_12_23/amenity-header.jpg" alt="">
    </div>
    <div class="gradient"></div>
    <div class="container hero-copy hero-copy-inner">
      <h2 class="hero-lrg-header">The Life<br>You Deserve</h2>
    </div>
  </section>

  <section>
    <div class="container">
      <p class="sub-hero-para oe-fadeinup">Carefully curated amenities provide perfect support throughout your day as you enjoy breezy transitions from home to work and back again without skipping a beat. World-class workouts, an exclusive resident lounge, premier co-working facilities, and so much more await you just moments from a nearby train in a community overflowing with opportunity.</p>

      <div class="flex-container">
        <div class="flex-img-container oe-fadeinup">
          <!-- <img src="images/amenities/Amen_2-courtyard.jpg" alt=""> -->
          <img src="images/new-img-may_5_23/may_12_23/118P-Amenity-Courtyard.jpg" alt="">
        </div>
        <div class="flex-copy-container">
          <h3 class="inner-page-section-header oe-fadeinup">Landscaped<br>Courtyard</h3>
          <p class="oe-fadeinup">Spread out and soak in the sun from a vibrant courtyard that provides fresh-air escapes and stunning spaces to socialize as you gather around the grill or fire pit, enjoy an al fresco meal, or play some ping pong.</p>
        </div>
      </div>

      <div class="flex-container row-reverse">
        <div class="flex-img-container tall oe-fadeinup">
          <img src="images/june_22_23/Amen_3-fitness.jpg" alt="">
        </div>
        <div class="flex-copy-container tall">
          <h3 class="inner-page-section-header oe-fadeinup">Fitness<br>Center</h3>
          <p class="oe-fadeinup">Get in a state-of-the-art workout at an impressively appointed fitness center that combines the latest machine workouts with cardio, free weights, and much more in a convenient and private setting just moments from your front door.</p>
        </div>
      </div>

      <!-- Core Swiper Without Styles -->
      <!-- Slider main container -->
      <div class="swiper oe-fadeinup">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
          <!-- Slides -->
          <div class="swiper-slide">
            <!-- <img src="images/amenities/Amen_Slider-1.jpg" alt=""> -->
            <img src="images/new-img-may_5_23/may_12_23/amenity-slider-pergola.jpg" alt="">
            <p>Pergola with Outdoor Dining Areas</p>
          </div>
          <div class="swiper-slide">
            <!-- <img src="images/amenities/Amen_Slider-2.jpg" alt=""> -->
            <img src="images/june_22_23/amenity-slider-cowork.jpg" alt="">
            <p>Co-Working Lounge & Clubroom</p>
          </div>
          <div class="swiper-slide">
            <!-- <img src="images/amenities/Amen_Slider-3.jpg" alt=""> -->
            <img src="images/new-img-may_5_23/may_12_23/amenity-slider-games.jpg" alt="">
            <p>Outdoor Courtyard with Fire Pits, BBQs & Games</p>
          </div>
        </div>
        <!-- If we need pagination -->
        <!-- <div class="swiper-pagination"></div> -->

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

        <!-- If we need scrollbar -->
        <!-- <div class="swiper-scrollbar"></div> -->

      </div>
    </div>
  </section>

  <section class="large-letters-grey-background">
    <div class="container">
      <h4 class="sml-header oe-fadeinup">
        Featured Amenities
      </h4>
      <div class="list-container">
        <h5 class="oe-fadeinup">Indoor Amenities</h5>
        <ul class="oe-fadeinup">
          <li>Butterfly Smart Phone/Video Intercom</li>
          <li>State-of-the-Art Fitness Center </li>
          <li>Resident Co-Work Lounge & Club Room</li>
          <li>Amazon Hub Locker System with Storage Lockers</li>
          <li>On-Site Management Bike Storage</li>
          <li>On-Site Maintenance</li>
          <li>WiFi in Common Areas </li>
          <!-- <li>Bike Storage</li> -->
          <li>Pet-Friendly</li>
          <li>Smoke-Free Community</li>
        </ul>
      </div>
      <div class="list-container">
        <h5 class="oe-fadeinup">Outdoor Amenities</h5>
        <ul class="oe-fadeinup">
          <li>Outside Courtyard with Private BBQ Grills & Fire&nbsp;Pit </li>
          <li>Pergola with Outdoor Dining Areas</li>
          <li>Outdoor Gaming with Foosball & Ping Pong Table</li>
          <li>Electric Car Charging Stations </li>
          <li>On-Site Parking</li>
        </ul>
      </div>
    </div>
  </section>

  <!-- <a href="./index.php">Home</a> -->


  <?php include('_footer.php') ?>
  <?php include('footer-scripts.php'); ?>
  <script>
    var tl = gsap.timeline();
    tl.to(".hero", {
      autoAlpha: 1,
      duration: 1
    });
    tl.to(".header-inner", {
      autoAlpha: 1,
      duration: 1
    }, "<");
    tl.to(".innerpage-header", {
      autoAlpha: 1,
      duration: 1.5
    }, "<");
    tl.from(".innerpage-header", {
      y: 30,
      duration: 1.5
    }, "<");
  </script>
</body>

</html>