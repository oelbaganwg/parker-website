<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title></title>
      <meta name="description" content="">
      <?php include('header-scripts.php'); ?>  
      <link
  rel="stylesheet"
  href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
/>
      <style>
        ul li:before {
          display: inline-block;
          content: '-';
          -webkit-border-radius: 0.375rem;
          border-radius: 0.375rem;
          height: 0.40rem;
          width: 0.40rem;
          margin-right: 0.5rem;
          position: absolute;
          top: 3px;
          left: 0px;
        }
        ul li {
          margin:15px auto;
          padding-left: 16px;
          position: relative;
          line-height: 124%;
        }
      </style>  
    </head>
    <body>
      
    <div class="oe-instructions">
      <h1>Usage</h1>
      <ul>
      <li>Use mobile first approach. All css should refer to mobile and then larger breakpoints can be specified with min-width only, (no max-width.) See  example in scss/styles.scss</li>
      <li>Keep website inside localhost root under folder name. 
        http:// localhost/sitename<br />Open terminal in that folder and run this to initiate build: npm install </li>
      <li>Push and commit to provided Git Repo when a revision is complete on the staging branch.</li>
      <li>Use SASS and nest breakpoints under each class as needed. Keep all .scss files in the scss folder and source js in one folder. Reset normalization CSS has already been added. There are a few classes already added in the scss folder. </li>
      <li>Fonts: We can use typekit or OTF's stored in the styles/font folder (and then update the css/fonts/stylesheet.css if custom fonts are used)</li>
      <li>Do not include any bootstrap css or Wordpress.</li>
      <li>Based on the gulp file, all js and css should be minified as one file in scripts and styles folder. This build will already do that. You can add more files to watch in the gulp file if needed. This build includes Swiper JS, and GSAP</li>
      <li>Use Semantic HTML 5 elements for all elements like: header, footer, main, section, article, h1, h2, p, ul li</li>
      <li>Use only one h1 tag on each page for the main headline and h tags on all headings</li>
      <li>Use &lt;img /&gt; tags with the object-fit css property instead of background-size for all photo placements</li>
      <li>Add alt tags to all &lt;img /&gt; elements: </li>
      <li>Use the .php extension on all pages and store in root or a single directory</li>
      <li> Keep all global navigation, header, footers and forms in a single php include file and include them on all pages with: &lt;?php include('header.php'); ?&gt;</li>
      <li>Use the form html structure from below as is because it submits to our internal API, but you can change the styles according to the design</li>
      <li>Use <a href="https://swiperjs.com/get-started" target="_blank">Swiperjs.com</a> for all sliders but style according to provided design. Swiper js and css is already minifed in this build and one instance is initialized in main.js file (Owl Carousel is also ok as long as the design is followed)</li>
      <li>Use <a href="https://developers.google.com/web/fundamentals/design-and-ux/responsive/images" target="_blank">picture and srcset</a> for all photo placements to appease SEO and so that we can use mobile specifc image sizes if needed: </li>
      <li>
        <pre>
        <code>
      &lt;picture&gt;
        &lt;source media="(min-width: 768px)" srcset="head.jpg, head-2x.jpg 2x">
        &lt;img src="head-fb.jpg" srcset="head-fb-2x.jpg 2x" alt="img description"&gt;
      &lt;/picture&gt;
      </code>
        </pre>
      </li>
    </ul>

      <!-- Core Swiper Without Styles -->
      <!-- Slider main container -->
      <div class="swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
          <!-- Slides -->
          <div class="swiper-slide">Slide 1</div>
          <div class="swiper-slide">Slide 2</div>
          <div class="swiper-slide">Slide 3</div>
        </div>
        <!-- If we need pagination -->
        <!-- <div class="swiper-pagination"></div> -->

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

        <!-- If we need scrollbar -->
        <!-- <div class="swiper-scrollbar"></div> -->
      </div>


          <!-- form -->
            <div class="holdthanks">
              Thank you for your inquiry.
            </div>
            <form id="theform" class="holdform">
              <div class="input">
                <label for="formdata_name">Full Name*</label>
                <input type="text" name="formdata_name" class="contactfields" />
              </div>

              <div class="input email">
                <label for="formdata_email">Email*</label>
                <input type="text" name="formdata_email" class="contactfields" />
              </div>

              <div class="input phone">
                <label for="formdata_phone">Phone</label>
                <input type="text" name="formdata_phone" class="contactfields" />
              </div>
             <!--  <div class="holdfields">
              </div> -->
              
              <div class="input">
                <label for="formdata_comments">Comments</label>
                <input type="text" name="formdata_comments" class="contactfields" />
              </div>
              <div class="formerror"></div>
              <div class="holdbtncontainerstatus" style="display:none;">Sending ...</div>
              <div class="holdbtn">
                <button class="btn" onclick="sendForm()" type="button">Submit</button>
              </div>
              <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
              <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
              <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
              <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
            </form>
          <!-- form --> 

      <script>
        var waitforthisimage = "images/x.jpg";
      </script>
      <?php include('footer-scripts.php'); ?>
      <script>
        
        //check if main image is loaded and run function
        if ($('video').length) { 

          // Start Is Video Ready
            var thevideo = document.getElementById("video");
            var videoid = 'video'; 
            $(function() {
                var onVideoReady = function(){
                  setTimeout(function(){
                    loadhero();
                    playvideo(thevideo);
                  }, 0); 
                }
                var onVideoError = function(){
                    loadhero();
                    playvideo(thevideo);
                    console.warn('Video failed to load');
                }
                var isVideoReady = new videoReady(videoid, onVideoReady, onVideoError);
                    isVideoReady.check();
            });
          // End Is Video Ready

          } else {
              var readyclass = "page_ready";
              var img = new Image();
              img.onload = function() {
                  loadhero();
              }
              img.onerror = function() {
                  loadhero();
              };
              img.src = waitforthisimage;
            }


        //gsap opening animation
        function loadhero() {
          // var heroLoad = gsap.timeline();
          // heroLoad.addLabel('go')
          // heroLoad.add( function(){ 
          //   $('body').addClass(readyclass)
          // },'go')
          // heroLoad.to('.logoarea', 1, {
          //   top:0,
          //   ease: "slow"
          // },'go')

        }

      
        //scroll trigger animation
        // const theformid = $('.holdform');
        // gsap.from(theformid, {
        //   duration: 1,
        //   // scale: 0.7,
        //   opacity:0,
        //   y: 30,
        //   stagger:1,
        //   scrollTrigger: {
        //     trigger:theformid,
        //     markers:false,
        //     start:"top 90%",
        //     end:"bottom 0%",
        //     // events: onEnter, onLeave, onEnterBack, onLeaveBack
        //     toggleActions:"play none none reverse"
        //     //options: play, pause, resume, reset, restart, complete, reverse, none
        //   },
        //   ease:"none"
        // });
        

        // parallax scrub
        // gsap.from('section.neighborhood', {
        //   duration: 1,
        //   y: 150,
        //   scrollTrigger: {
        //     trigger:'section.neighborhood',
        //     scrub: 0.4, // "true" full scrub
        //     markers:false,
        //     start:"top 90%",
        //     end:"bottom 0%",
        //     toggleActions:"play none none reverse",
        //   },
        //   ease:"none"
        // });


        //built simple fade in
        //  $( ".oe-fadeinup" ).each(function( index ) {
        //   console.log(this);
        //   gsap.from(this, {
        //     duration: 1,
        //     // scale: 0.7,
        //     opacity:0,
        //     y: 30,
        //     stagger:1,
        //     scrollTrigger: {
        //       trigger:this,
        //       markers:false,
        //       start:"top 90%",
        //       end:"bottom 0%",
        //       // events: onEnter, onLeave, onEnterBack, onLeaveBack
        //       toggleActions:"play none none reverse"
        //       //options: play, pause, resume, reset, restart, complete, reverse, none
        //     },
        //     ease:"none"
        //   });
        // });


        // More complex Detect section entrance
        // ScrollTrigger.create({
        //   trigger: ".holdvideo",
        //   start:"top 90%",
        //   end:"bottom 0%",
        //   onEnter: () => playVimeo(true),
        //   onLeave: () => playVimeo(false),
        //   onEnterBack: () => playVimeo(true),
        //   onLeaveBack: () => playVimeo(false),
        //   markers:false
        // });

      </script>
    </body>
  </html>