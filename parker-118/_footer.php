<footer class="footer">
  <div class="container">
    <address>
      <div>118 Park Avenue, Rutherford, NJ 07070</div>
      <div>
      <a href="tel:201-933-2106">201.933.2106</a>  <span>/</span><a href="mailto:info@renttheparker.com">Info@RentTheParker.com</a></div>
    </address>
    <div class="social-icons-container">
      <a href="https://www.facebook.com/TheParker118/" target="_blank">
        <img class="social-icon" src="images/footer/facebook-logo.svg" alt="">
      </a>
      <a href="https://www.instagram.com/theparker118/" target="_blank">
        <img class="social-icon" src="images/footer/instagram-logo.svg" alt="">
      </a>
    </div>
    <div class="footer-logos-container">
      <img src="images/footer/Vango-gray.svg" alt="" class="footer-logo">
      <a href="https://newworldgroup.com" target="_blank"><img src="images/MadeByNWG-square.svg" alt="" class="footer-logo nwg"></a>
      <img src="images/footer/EHO-gray.svg" alt="" class="footer-logo">
      <img src="images/footer/ADA-gray.svg" alt="" class="footer-logo">
    </div>
    <div class="disclaimer">
    <!-- *Contact our leasing team for offer lease terms.<br> -->
    All images are a combination of photography and artist renderings. The artist representation, Interior decorations, finishes, appliances and furnishings are provided for illustrative purposes&nbsp;only.
    <!-- All dimensions are approximate and subject to normal construction variances, and tolerances. Plans may contain minor variations from floor to floor. -->
     <a href="./privacypolicy" target="_blank">Privacy&nbsp;Policy</a> 
    </div>
  </div>
</footer>