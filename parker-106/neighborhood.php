<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>New Luxury Rentals in Rutherford | Apartments for Rent in NJ</title>
    <meta name="description" content="The Parker is located in the best neighborhood in New Jersey. Steps from the Rutherford Train Station. On-Site Parking. Bike Storage. Pet Friendly. Now Leasing.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="neighborhood">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/493075581-parker106-neighborhood-header.jpeg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">HOT SPOT LIVING</h1>
        <p>The Neighborhood</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- info-section -->
<section class="info-section">
    <div class="col-46 order-md-2">
        <div class="container">
            <div class="info-img" data-aos="fade-up">
                <img src="images/20190712_109ParkAveRutherford-2653.jpg" alt="#">
            </div>
            <div data-aos="fade-up">
                <h3 class="block-title">LIFE ON PARK</h3>
                <p>Be in the center of it all. In the mood for Italian? We’ve got you covered. Craving crispy bacon or sweet French toast? Five minutes away. Want to grab a drink and catch the next
                    big game? Take a stroll with your four-legged friend? Grab a coffee and head into the city? No worries! It's all here in your corner of the world. Food, festivals, shopping,
                    Memorial Park, Meadowlands Entertainment Center and so much more, all at your doorstep.</p>
            </div>
        </div>
    </div>
    <div class="col-54">
        <div class="img-block" data-aos="fade">
            <img src="images/20190712_109ParkAveRutherford-2716.jpg" alt="#" class="full-img">
        </div>
    </div>
</section>

<!-- map-section -->
<section class="map-section" data-lat="40.82718" data-lng="-74.105462" data-marker="images/map-parker.png" data-title="Parker">
    <div class="map-container" data-aos="fade">
        <div id="map" class="map"></div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK3vp71vqv_uF_rNu45tCqBwlvEs8HOSQ"></script>
        <script>

            jQuery(document).ready(function($){

                var section_map = $('.map-section');
                var blocks_map_lat = section_map.data('lat') * 1;
                var blocks_map_lng = section_map.data('lng') * 1;
                var blocks_map_title = section_map.data('title');
                var blocks_map_marker = section_map.data('marker');
                var blocks_map = $('.map-block');

                var markers = [];
                var map = [];
                var lat = blocks_map_lat;
                var lng = blocks_map_lng;
                var activeInfoWindow;
                var zoom = 17;

                function initMap() {

                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: lat, lng: lng},
                        zoom: zoom,
                        disableDefaultUI: true,
                        gestureHandling: 'greedy',
                        scrollwheel: false,
                        zoomControl:true,
                        styles: [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#fcfcfc"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#7b7d8b"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eef1f9"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eef1f9"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#7b7d8b"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#c4c9d4"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#7b7d8b"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#54565a"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c4c9d4"
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#8e8fa1"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#63656c"
                                    },
                                    {
                                        "weight": 1.5
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#63656c"
                                    },
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "weight": 2
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station.rail",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ff292e"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station.rail",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "weight": 1.5
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dee2ef"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    },
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            }
                        ]
                    });

                    $.each(blocks_map, function(i, el){

                        var elLiA = $('.showItemOnMap', el);

                        $.each(elLiA, function(index, elLiThis){

                            var marker_lat = $(this).data('lat') * 1;
                            var marker_lng = $(this).data('lng') * 1;

                            $(this).data('marker_index', index);

                            var item = {
                                position: {lat: marker_lat, lng: marker_lng},
                                map: map,
                                type: $(el).data('block'),
                                icon: $(el).data('marker'),
                                index: index,
                                message: $(this).data('title'),
                            };

                            var marker = new google.maps.Marker(item);

                            marker.addListener('click', function () {
                                showInfoWindow(marker);
                            });

                            markers.push(marker);
                        });
                    });

                    var item = {
                        position: {lat: blocks_map_lat, lng: blocks_map_lng},
                        map: map,
                        type: 'default',
                        icon: blocks_map_marker,
                        message: blocks_map_title,
                    };

                    var marker = new google.maps.Marker(item);

                    if (blocks_map_title.length > 0) {
                        marker.addListener('click', function () {
                            showInfoWindow(marker);
                        });
                    }

                    markers.push(marker);

                }

                function showInfoWindow(marker) {
                    if (activeInfoWindow) {
                        activeInfoWindow.close();
                    }

                    var infowindow = new google.maps.InfoWindow({content: marker.message});

                    if($(window).width() < 768) {
                    }
                    map.setCenter(marker.getPosition());
                    infowindow.open(map, marker);
                    activeInfoWindow = infowindow;
                }

                function filterMarkers(category) {

                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < markers.length; i++) {
                        marker = markers[i];
                        if (marker.type == category || category.length == 0 || category === false || marker.type == 'default') {
                            marker.setVisible(true);
                            bounds.extend(markers[i].getPosition());
                        } else {
                            marker.setVisible(false);
                        }
                    }

                    if($(window).width() >= 768) {
                        //map.fitBounds(bounds);
                        //map.setCenter(marker.getPosition());
                    }
                }

                $(document).on('click', '.showItemOnMap', function (event) {
                    event.preventDefault();

                    var thCategory = $(this).closest('.map-block').data('block');

                    filterMarkers(thCategory);

                    for (var i = 0; i < markers.length; i++) {
                        marker = markers[i];
                        if (marker.type == thCategory && marker.index == $(this).data('marker_index')) {
                            showInfoWindow(marker);
                        }
                    }

                });

                initMap();
            });

        </script>
    </div>
    <div class="map-info" data-aos="fade">
        <div class="map-info-head">
            <h3 class="map-title">THE NEIGHBORHOOD</h3>
        </div>
        <div class="map-info-body scroll-block">
            <div class="map-block restaurants" data-block="restaurants" data-marker="images/map-restaurants.png">
                <h3 class="map-block-title">DINING</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.8273011" data-lng="-74.1081721" data-marker_index="" data-title="Song E’ Napule">Song E’ Napule</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8273011" data-lng="-74.1081721" data-marker_index="" data-title="Steven’s Café">Steven’s Café</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="" data-lng="" data-marker_index="" data-title="Salad House (coming soon)">Salad House (coming soon)</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8250243" data-lng="-74.1093455" data-marker_index="" data-title="Cafe Matisse">Cafe Matisse</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8272488" data-lng="-74.1050017" data-marker_index="" data-title="Mambo Tea House">Mambo Tea House</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8273142" data-lng="-74.1045649" data-marker_index="" data-title="The Risotto House">The Risotto House</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8273116" data-lng="-74.1038578" data-marker_index="" data-title="Yamada Sushi">Yamada Sushi</a></li> -->
                    <li><a href="" class="showItemOnMap" data-lat="40.829124" data-lng="-74.100154" data-marker_index="" data-title="The New Park Tavern">The New Park Tavern</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8327032" data-lng="-74.0952209" data-marker_index="" data-title="Candlewyck Diner">Candlewyck Diner</a></li> -->
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8281358" data-lng="-74.0973171" data-marker_index="" data-title="Elia">Elia</a></li> -->
                    
                    <li><a href="" class="showItemOnMap" data-lat="40.8281969" data-lng="-74.1027362" data-marker_index="" data-title="Rutherford Pancake House">Rutherford Pancake House</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8276351" data-lng="-74.1038377" data-marker_index="" data-title="Matera's On Park">Matera's On Park</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8280447" data-lng="-74.1013627" data-marker_index="" data-title="Volares">Volares</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8274696" data-lng="-74.1044046" data-marker_index="" data-title="Playa Bowls">Playa Bowls</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.827915" data-lng="-74.1032643" data-marker_index="" data-title="Dunkin'">Dunkin'</a></li>
                    <!-- <li><a href="" class="showItemOnMap" data-lat="40.8267182" data-lng="-74.1061401" data-marker_index="" data-title="Paisano's">Paisano's</a></li> -->
                    <li><a href="" class="showItemOnMap" data-lat="40.8285672" data-lng="-74.1018557" data-marker_index="" data-title="Ara Coffee">Ara Coffee</a></li>

                    <li><a href="" class="showItemOnMap" data-lat="40.82729833054388" data-lng="-74.10470094642778" data-marker_index="" data-title="Suprema">Suprema</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.827354464422385" data-lng="-74.10490831554792" data-marker_index="" data-title="Fiorentini">Fiorentini</a></li>
                    
                    <li><a href="" class="showItemOnMap" data-lat="40.828083690310244" data-lng="-74.10395318875584" data-marker_index="" data-title="Erie Coffeeshop & Bakery">Erie Coffeeshop & Bakery</a></li>
                </ul>
            </div>

          <div class="map-block school" data-block="school" data-marker="images/map-schools.png">
                <h3 class="map-block-title">SCHOOLS</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.8331009" data-lng="-74.106607" data-marker_index="" data-title="Washington School">Washington School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8290208" data-lng="-74.1082134" data-marker_index="" data-title="Rutherford High School">Rutherford High School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.821247" data-lng="-74.1111338" data-marker_index="" data-title="Pierrepont School">Pierrepont School</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8260379" data-lng="-74.108424" data-marker_index="" data-title="Rutherford Public Schools">Rutherford Public Schools</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8240994" data-lng="-74.1058587" data-marker_index="" data-title="Kindergarten Center">Kindergarten Center</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.82664" data-lng="-74.10735" data-marker_index="" data-title="Goldilocks Children Learning Center">Schoolhouse Early Learning LLC</a></li>

                </ul>
            </div>

            <div class="map-block lifestyle" data-block="lifestyle" data-marker="images/map-lifestyle.png">
                <h3 class="map-block-title">LIFESTYLE</h3>
                <ul>
                     <li><a href="" class="showItemOnMap" data-lat="40.8262763" data-lng="-74.1077909" data-marker_index="" data-title="United States Postal Service">United States Postal Service</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.823177" data-lng="-74.099625" data-marker_index="" data-title="Club Metro USA">Club Metro USA</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.829938" data-lng="-74.0994319" data-marker_index="" data-title="Dog House Bakery & Grooming Salon">Dog House Bakery & Grooming Salon</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8279532" data-lng="-74.1031747" data-marker_index="" data-title="Substance Salon & Barberspa">Substance Salon & Barberspa</a></li>

                     <li><a href="" class="showItemOnMap" data-lat="40.8280996" data-lng="-74.0997138" data-marker_index="" data-title="Strong & Shapely Gym">Strong & Shapely Gym</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8279655" data-lng="-74.1017663" data-marker_index="" data-title="Personal Touch Fitness">Personal Touch Fitness</a></li>
                     <li><a href="" class="showItemOnMap" data-lat="40.8277164" data-lng="-74.0998621" data-marker_index="" data-title="Untamed Boxing And Fitness">Untamed Boxing And Fitness</a></li>
                </ul>
            </div>

            <div class="map-block entertainment" data-block="entertainment" data-marker="images/map-entertainment.png">
                <h3 class="map-block-title">ENTERTAINMENT</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.814639" data-lng="-74.1040694" data-marker_index="" data-title="Meadowlands Museum">Meadowlands Museum</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.82708" data-lng="-74.102933" data-marker_index="" data-title="Center Cinemas">Center Cinemas</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8277975" data-lng="-74.1040201" data-marker_index="" data-title="Paint Zone Art Studio">Paint Zone Art Studio</a></li>
                </ul>
            </div>

            <div class="map-block shop" data-block="shop" data-marker="images/map-shopping.png">
                <h3 class="map-block-title">SHOPPING</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.8243284" data-lng="-74.1124286" data-marker_index="" data-title="Rutherford Wine Shoppe">Rutherford Wine Shoppe</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8270426" data-lng="-74.1045279" data-marker_index="" data-title="Rutherford Florist">Rutherford Florist</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8278126" data-lng="-74.1034181" data-marker_index="" data-title="Varrelmann's Bake Shop">Varrelmann's Bake Shop</a></li>

                    <li><a href="" class="showItemOnMap" data-lat="40.8311036" data-lng="-74.0932545" data-marker_index="" data-title="Liberty Commons LLC">Liberty Commons LLC</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8308289" data-lng="-74.0898786" data-marker_index="" data-title="Rutherford Commons">Rutherford Commons</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8234437" data-lng="-74.1140313" data-marker_index="" data-title="Krauszer's Food and Liquor Store">Krauszer's Food and Liquor Store</a></li>
                </ul>
            </div>

            <div class="map-block park" data-block="park" data-marker="images/map-parks.png">
                <h3 class="map-block-title">PARK</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.8381911" data-lng="-74.1156216" data-marker_index="" data-title="Sunset Park">Sunset Park</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8259796" data-lng="-74.0979349" data-marker_index="" data-title="Riggin Memorial Field">Riggin Memorial Field</a></li>
                    <li><a href="" class="showItemOnMap" data-lat="40.8216893" data-lng="-74.1014221" data-marker_index="" data-title="Wall Field">Wall Field</a></li>
                </ul>
            </div>

            <div class="map-block school" data-block="transit" data-marker="images/map-transit.png">
                <h3 class="map-block-title">TRANSIT</h3>
                <ul>
                    <li><a href="" class="showItemOnMap" data-lat="40.828277" data-lng="-74.1008057" data-marker_index="" data-title="Rutherford Train Station">Rutherford Train Station</a></li>
                </ul>
            </div>
            <br>
            <br>
        </div>
    </div>
</section>

<?php include('_footer.php') ?>