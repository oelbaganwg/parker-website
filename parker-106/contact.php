<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Apartments NJ | Luxury Apartments for Rent in Rutherford </title>
    <meta name="description" content="Studio to 2-bedroom rentals in NJ with premier amenities. Fitness Center. Resident Lounge. Club Room. On-site Parking.  Now Leasing. Contact us today. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="contacts">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<div class="contacts-section">
    <div class="contacts-block">
        <h2 class="contacts-title">CONTACT US</h2>
        <p>Don't miss this opportunity to be part of The Parker, Rutherford's most highly-anticipated
            residential community.<span class="pleaseregister"> Please register below to learn more, and a member of our leasing team will be in touch as soon as possible.</span></p>
        <!-- <p style="line-height: 1.5">Leasing Office Hours:<br>
            Monday - Friday: 10 am to 6pm | Saturday & Sunday: 11am to 5pm</p> -->
        <p><a href="https://goo.gl/maps/KFYFayqAA2w" class="under-link" target="_blank">Get Directions.</a></p>
        <p class='holdthanks'>Thank you for your Inquiry.</p>
        <div class="form-block holdform">
            <form id="theform">
                <div class="form-froup">
                    <input id="formdata_name" name="formdata_name" type="text" class="form-control" placeholder="NAME*" required>
                </div>
                <div class="form-froup">
                    <input id="formdata_email" name="formdata_email"  type="email" class="form-control" placeholder="EMAIL*" required>
                </div>
                <div class="form-froup">
                    <input id="formdata_phone" name="formdata_phone" type="tel" class="form-control" placeholder="Phone">
                </div>
                <div id="formerror"></div>
                <div class="form-footer">
                    <button class="btn-link formsubmit" type="button">SUBMIT</button>
                </div>
                
                <input type="hidden" name="domainAccountId" value="LAS-337185-55" />
                <input type="hidden" name="guid" value="" />
                <input type="hidden" name="formdata_property" value="The Parker 106" />
                <input id="formdata_comments" name="formdata_comments" type="hidden" value="">
                  <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
            </form>
        </div>
    </div>
</div>
<script src="//app.lassocrm.com/analytics.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
</script>

<?php include('_footer.php')?>