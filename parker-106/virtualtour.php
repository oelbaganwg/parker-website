<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>Virtual Tours | Luxury Rentals in New Jersey | Brand New Rutherford Residences </title>
    <meta name="description" content="Studio to 2 bedroom rentals. Resort-style amenities. Private fitness center. On-site garage parking. Resident lounge and club room. Steps from the Rutherford Train Station. Now Leasing. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css?v1"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>
    <style>
        section.iframearea  {
                width:100%;
                height:300px;
                text-align: center;
                margin-bottom: 100px;
        }
        section.iframearea iframe {
                border: 0;
                width: 100%;
                margin: 0 auto;
                height: 100%;
                text-align: center;
        }
        section.iframearea .holdcontent {
                width:90%;
                height: 100%;
                margin:0 auto;
        }
        @media (min-width: 768px){

            section.iframearea  {
                    height:700px;
            }

        }
        @media (min-width: 992px){

            section.iframearea  {
                    height:800px;
            }

        }
        @media (min-width: 1200px){

            section.iframearea  {
                    min-height:800px;
                    height:80vh;
            }

        }
    </style>

</head>
<body class="virtualtourpage">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <!-- <img src="images/amen-1.jpg" alt="#" class="full-img"> -->
    <img src="images/TheParker16.jpg" alt="#" class="full-img-oe amenities">
    <div class="container">
        <h1 class="primary-title">PEEK INSIDE</h1>
        <p>Virtual Tour</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>



<!-- sub-section -->
<section class="iframearea" data-aos="fade" style="margin-top:20px;">
    <div class="holdcontent">
        <!-- <h3 class="block-title" style="text-transform: uppercase;text-align: left;">Virtual Tour</h3> -->
         <!--    <iframe
    src="//walkinto.in/tour/bJ9Eu8ZPsr-Jeq4uUWvoB"
    width="100%"
    height="600"
    allowfullscreen="true"
    webkitallowfullscreen="true"
    mozallowfullscreen="true"
    oallowfullscreen="true"
    msallowfullscreen="true"
    frameborder="0"
    scrolling="no"
    marginheight="0"
    marginwidth="0"> -->
     <iframe webkitAllowFullScreen mozallowfullscreen allowFullScreen 
     src="https://live.tourdash.com/embed/fbb579c60b3a4013b83a23b841cfc228?bg=rgba(26,172,214,1)&fg=rgba(255,255,255,1)&txt=Tap%20to%20view%20the%20virtual%20tour" 
     frameBorder="0" 
     width="100%"
    height="600"
    allowfullscreen="true"
    webkitallowfullscreen="true"
    mozallowfullscreen="true"
    oallowfullscreen="true"
    msallowfullscreen="true"
    frameborder="0"
    scrolling="no"
    marginheight="0"
    marginwidth="0"

     ></iframe>
</iframe>

    </div>
</section>
<!-- sub-section -->
<!-- <section class="sub-section" data-aos="fade">
    <img src="images/TheParker23.jpg" alt="#" class="full-img-oe amenities">
</section> -->

<?php include('_footer.php') ?>

