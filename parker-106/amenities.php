<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>Luxury Rentals in New Jersey | Brand New Rutherford Residences </title>
    <meta name="description" content="Studio to 2 bedroom rentals. Resort-style amenities. Private fitness center. On-site garage parking. Resident lounge and club room. Steps from the Rutherford Train Station. Now Leasing. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css?v1"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>
    <style>
        section.iframearea  {
                width:100%;
                height:300px;
                text-align: center;
                margin-bottom: 100px;
        }
        section.iframearea iframe {
                border: 0;
                width: 100%;
                margin: 0 auto;
                height: 100%;
                text-align: center;
        }
        section.iframearea .holdcontent {
                width:90%;
                height: 100%;
                margin:0 auto;
        }
        @media (min-width: 768px){

            section.iframearea  {
                    height:700px;
            }

        }
        @media (min-width: 992px){

            section.iframearea  {
                    height:800px;
            }

        }
        @media (min-width: 1200px){

            section.iframearea  {
                    min-height:800px;
                    height:80vh;
            }

        }
    </style>

</head>
<body class="amenities">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <!-- <img src="images/amen-1.jpg" alt="#" class="full-img"> -->
    <img src="images/TheParker15.jpg" alt="#" class="full-img-oe amenities">
    <div class="container">
        <h1 class="primary-title">VIP STYLE</h1>
        <p>The Amenities</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- info-section -->
<section class="info-section">
    <div class="col-46">
        <div class="container">
            <div class="info-img" data-aos="fade-up">
                <img src="images/TheParker22.jpg" alt="#">
            </div>
            <div class="info-body" data-aos="fade-up">
                <h3 class="block-title">WHERE LIFE HAS NO LIMITS</h3>
                <p>The luxury rentals at this premier collection overﬂow with opportunity, options and all around optimal living. The Parker boasts a private ﬁtness center, an exclusive resident lounge and club room, an exquisitely designed lobby to welcome you home and on-site parking. All this...only moments from the Rutherford Train Station. </p>
            </div>
            <div data-aos="fade-up">
                <h4 class="sub-title">AMENITY FEATURES:</h4>
                <ul>
                    <li>Butterﬂy Smart Phone/Video Intercom</li>
                    <li>State-of-the-Art Fitness Center</li>
                    <li>Resident Lounge/Club Room</li>
                    <li>On-site Parking</li>
                    <li>WiFi in Common Areas</li>
                    <li>Bike Storage</li>
                    <li>Pet-Friendly</li>
                    <li>Smoke-Free Community</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-54">
        <div class="img-block" data-aos="fade">
            <!-- <img src="images/NEW_Fitness.jpg" alt="#" class="full-img"> -->
            <img src="images/TheParker-04s-EDIT-A.png" alt="#" class="full-img">
        </div>
    </div>
</section>

<!-- sub-section -->
<section class="iframearea" data-aos="fade">
    <div class="holdcontent">
        <h3 class="block-title" style="text-transform: uppercase;text-align: left;">Virtual Tour</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!4v1574476283314!6m8!1m7!1sCAoSLEFGMVFpcE5iamVWRlBvNXNLbWpHdGU2Zk5ITWdNV2hxWHhoOWI2ODhwQWgt!2m2!1d40.82704404945854!2d-74.1054153!3f335.34828411083816!4f-1.942387845934789!5f0.7820865974627469" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</section>
<!-- sub-section -->
<section class="sub-section" data-aos="fade">
    <img src="images/TheParker23.jpg" alt="#" class="full-img-oe amenities">
</section>

<?php include('_footer.php') ?>

