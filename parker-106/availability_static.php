<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Apartments For Rent | Studio to 2 Bedroom Rentals in NJ</title>
    <meta name="description" content="Spacious studio, 1-, and 2-bedroom rental residences at The Parker. Convenient Location Near Rutherford Train Station. On-Site Covered Parking. Premier Amenities. Now Leasing.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="availability">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/Avail-1.jpg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">THE CHOICE IS YOURS</h1>
        <p>Availability</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- room-section -->
<section class="room-section">
    <div class="container">
        <ul class="tab-nav floorplansnav" data-aos="fade-up">
            <li class="tabs-item tabs-item-active"><a href="#"  data-id="all">ALL</a></li>
            <li class="tabs-item"><a href="#" data-id="0bed">STUDIO</a></li>
            <li class="tabs-item"><a href="#" data-id="1bed">ONE BEDROOM</a></li>
            <li class="tabs-item"><a href="#" data-id="2bed">TWO BEDROOM</a></li>
        </ul>
        <table class="table sortable table_avail" data-aos="fade-up">
            <thead>
            <tr>
                <!-- <th data-sort="float">Floor <img src="images/down-arrow.svg" alt="#"></th> -->
                <th data-sort="string">Bed/Bath <img src="images/down-arrow.svg" alt="#"></th>
                <th data-sort="float">Sq. Ft. <img src="images/down-arrow.svg" alt="#"></th>
                <th data-sort="float">Price <img src="images/down-arrow.svg" alt="#"></th>
                <th>Floorplan</th>
                <th ></th>
            </tr>
            <tbody>
            <tr class="0bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath">Studio / 1 Bath</td>
                <td data-label="Sq. Ft.">824 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal" class="btn btn-default" data-modal="#modal">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath">1 Bed / 1 Bath</td>
                <td data-label="Sq. Ft.">978 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">3</td> -->
                <td data-label="Bed/Bath">1 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,024 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">3</td> -->
                <td data-label="Bed/Bath">1 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,133 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="2bed">
                <!-- <td data-label="Floor">3</td> -->
                <td data-label="Bed/Bath">2 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,301 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="2bed">
                <!-- <td data-label="Floor">3</td> -->
                <td data-label="Bed/Bath">2 Bed / 2.5 Bath</td>
                <td data-label="Sq. Ft.">1,426 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="0bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath">Studio / 1 Bath</td>
                <td data-label="Sq. Ft.">824 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal" class="btn btn-default" data-modal="#modal">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath">1 Bed / 1 Bath</td>
                <td data-label="Sq. Ft.">978 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">3</td> -->
                <td data-label="Bed/Bath">1 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,024 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="1bed">
                <!-- <td data-label="Floor">1</td> -->
                <td data-label="Bed/Bath">1 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,133 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="2bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath">2 Bed / 2 Bath</td>
                <td data-label="Sq. Ft.">1,301 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            <tr class="2bed">
                <!-- <td data-label="Floor">1</td> -->
                <td data-label="Bed/Bath">2 Bed / 2.5 Bath</td>
                <td data-label="Sq. Ft.">1,426 sq. ft.</td>
                <td data-label="Price">X,XXX*</td>
                <td class="view-floor"><a href="#modal2" class="btn btn-default" data-modal="#modal2">View Floorplan</a></td>
                <td><a href="" class="btn btn-primary">Apply</a></td>
            </tr>
            </tbody>
        </table>
    </div>
</section>

<!-- Modal -->
<div id="modal" class="modal">
    <div class="modal-content">
        <div class="modal-left">
            <div class="modal-logo">
                <img src="images/logo-white-sm.svg" alt="#">
            </div>
            <div class="floorplan-desc">
                <h3 class="room-title">RESIDENCE 07</h3>
                <div>Floors 1-3</div>
                <div>Studio / 1 Bath</div>
                <div>824 sq. ft.</div>
            </div>
        </div>
        <div class="modal-right">
            <div class="floorplan-img">
                <img src="images/room.jpg" alt="#">
            </div>
        </div>
        <div class="modal-btn">
            <a href="" class="btn btn-primary">Apply</a>
            <a href="" class="btn btn-primary">Download</a>
        </div>
    </div>
</div>

<div id="modal2" class="modal">
    <div class="modal-content">
        <div class="modal-left">
            <div class="modal-logo">
                <img src="images/logo-white-sm.svg" alt="#">
            </div>
            <div class="floorplan-desc">
                <h3 class="room-title">RESIDENCE 07</h3>
                <div>Floors 1-3</div>
                <div>Studio / 1 Bath</div>
                <div>824 sq. ft.</div>
            </div>
        </div>
        <div class="modal-right">
            <div class="floorplan-img">
                <img src="images/room2.jpg" alt="#">
            </div>
        </div>
        <div class="modal-btn">
            <a href="" class="btn btn-primary">Apply</a>
            <a href="" class="btn btn-primary">Download</a>
        </div>
    </div>
</div>

<?php include('_footer.php')?>