<footer class="footer">
    <address>106 Park Ave, Rutherford, NJ 07070 | <a href="mailto:Info@RentTheParker.com">Info@RentTheParker.com</a> |
        <a href="tel:201.933.2106">201.933.2106</a></address>
    <div class="logos">
        <div class="logos-block">
            <img src="images/vango.svg" alt="Vango Development" width="55" height="25">
            <!-- <img src="images/tmd_logo.svg" alt="The Marketing Directors"> -->
            <img src="images/eho_logo.svg" alt="Equal Opportunity Housing" style="width: 22px;">
        </div>
        <div class="logos-text">
           All dimensions are approximate and subject to normal construction variances, and tolerances. Plans may contain minor variations from floor to floor.
        </div>
    </div>
</footer>

<script type='text/javascript' src="js/fullpage.min.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js.map"></script>
<script type='text/javascript' src="js/jquery.nicescroll.min.js"></script>
<script type='text/javascript' src="js/jquery.fancybox.min.js"></script>
<script type='text/javascript' src="js/stupidtable.min.js"></script>
<script type='text/javascript' src="js/imagesloaded.pkgd.min.js"></script>
<script type='text/javascript' src="js/isotope.pkgd.min.js"></script>
<script type='text/javascript' src="js/jquery.modal.js"></script>
<script type='text/javascript' src="js/aos.js"></script>
<script type='text/javascript' src="js/script.js"></script>
<script type='text/javascript' src="scripts/all.min.js"></script>
<script src="//app.lassocrm.com/analytics.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
var LassoCRM = LassoCRM || {};
(function(ns){
    ns.tracker = new LassoAnalytics('LAS-337185-55');
})(LassoCRM);
try {
    LassoCRM.tracker.setTrackingDomain("//app.lassocrm.com");
    LassoCRM.tracker.init();  // initializes the tracker
    LassoCRM.tracker.track(); // track() records the page visit with the current page title, to record multiple visits call repeatedly.
    LassoCRM.tracker.patchRegistrationForms();
} catch(error) {}
-->
</script>

</body>
</html>