<?php 
include('domain.php');
  // $response = file_get_contents('https://nestiolistings.com/api/v2/listings/all/?key=00f35fae3c514e0e8ba688c3914d9112');
  $response = file_get_contents('data/floorplans.json');

  //bela: 00f35fae3c514e0e8ba688c3914d9112
  //Skye API: 182dd104aabd42c49a394f4514bc022a
  //Parker API: ed76f328602546a88b3a2c5a92744b01

  $properties = json_decode($response);
  // var_dump($properties);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Apartments For Rent | Studio to 2 Bedroom Rentals in NJ</title>
    <meta name="description" content="Spacious studio, 1-, and 2-bedroom rental residences at The Parker. Convenient Location Near Rutherford Train Station. On-Site Covered Parking. Premier Amenities. Now Leasing.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

    <style>
    .availability .location-button-container {
      margin: 5rem 0;
      text-align: center;
  }

  .availability .location-button-container .location-btn {
    background: #fff;
    color: rgb(35, 31, 32);
    padding: 10px 30px 8px 38px;
    font-family: "Knockout";
    font-size: 25px;
    letter-spacing: 8px;
    line-height: normal;
    -webkit-transition: 0.3s all ease-in-out;
    -moz-transition: 0.3s all ease-in-out;
    -ms-transition: 0.3s all ease-in-out;
    -o-transition: 0.3s all ease-in-out;
    transition: 0.3s all ease-in-out;
    border: 1px solid rgb(35, 31, 32);
    display: inline-block;
    text-transform: uppercase;
    
  }

  .availability .location-button-container .location-btn:first-child {
    
    margin-bottom: 1rem;


  }
  
  .availability .location-button-container .active,
  .availability .location-button-container .location-btn:hover
   {
    background-color: black;
    color: white;
  }
  
  @media only screen and (min-width: 450px) {
    .availability .location-button-container .location-btn:first-child {
    
    margin-right: 1rem;
    margin-bottom: 0;


  }
  }
  
  </style>
</head>
<body class="availability">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/TheParker-11-EDIT.jpg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">THE CHOICE IS YOURS</h1>
        <p>Availability</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- room-section -->
<!-- <section class="room-section"> -->
    <!-- <div class="container">
        <ul class="tab-nav floorplansnav" data-aos="fade-up">
            <li class="tabs-item tabs-item-active"><a href="#"  data-id="all">ALL</a></li>
            <li class="tabs-item"><a href="#" data-id="0bed">STUDIO</a></li>
            <li class="tabs-item"><a href="#" data-id="1bed">ONE BEDROOM</a></li>
            <li class="tabs-item"><a href="#" data-id="2bed">TWO BEDROOM</a></li>
        </ul>
        <table class="table sortable table_avail" data-aos="fade-up">
            <thead>
            <tr>
                <th data-sort="float">Residence <img src="images/down-arrow.svg" alt="#"></th>
                <th data-sort="string">Bed/Bath <img src="images/down-arrow.svg" alt="#"></th>
                <th data-sort="float">Sq. Ft. <img src="images/down-arrow.svg" alt="#"></th>
                <th data-sort="float">Price <img src="images/down-arrow.svg" alt="#"></th>
                <th>Floorplan</th>
                <th >Apply</th>
            </tr>
            <tbody>

                  < ?php
                      foreach ($properties as $property) :
                        // var_dump($property);
                        $floor = $property->floor;
                        $residence = $property->residence;
                        $bedrooms = $property->bed;
                        $bathrooms = $property->bath;
                        $sqft = $property->sqft;
                        $price = $property->price;
                        $floorplan = $property->plan;
                        $available = $property->available;
                        $bedslabel = NULL;
                          switch ($bedrooms) {
                              case "0":
                                  $bedslabel = "Studio";
                                  break;
                              case "1":
                                  $bedslabel = "1 Bedroom";
                                  break;
                              case "2":
                                  $bedslabel = "2 Bedroom";
                                  break;
                              default:
                                  $bedslabel = "Studio";
                          }
                        $bedbathLabel = $bedslabel . " / " . $bathrooms . " Bath";
                        // $applynowlink = $property->building->application_link;
                        $price = number_format($property->price);
                        if ($available == "true") {
                    ?>

            <tr class="< ?php echo $bedrooms; ?>bed">
                <td data-label="Residence">< ?php echo $residence; ?></td>
                <td data-label="Bed/Bath">< ?php echo $bedbathLabel; ?></td>
                <td data-label="Sq. Ft.">< ?php echo $sqft; ?> sq. ft.</td>
                <td data-label="Price">$< ?php echo $price; ?></td>
                <td class="view-floor"><a href="data/floorplanpdfs/< ?php echo $floorplan; ?>" class="btn btn-default" target="_blank">View Floorplan</a></td>
                 <td class="view-floor"><a href="#modal" class="btn btn-default" data-modal="#modal">View Floorplan</a></td> 
                <td><a href="https://www.on-site.com/apply/property/275856" class="btn btn-primary"  target="_blank">Apply</a></td>
            </tr>

                    < ?php
                  }
                       endforeach; ?>
            </tbody>
        </table>
    </div> -->
    <!-- <script type='text/javascript' charset='utf-8'>
  document.write(unescape("%3Cscript src='" + (('https:' == document.location.protocol) ? 'https:' : 'http:') + "//vangorealestate.appfolio.com/javascripts/listing.js' type='text/javascript'%3E%3C/script%3E"));
</script>
 
<script type='text/javascript' charset='utf-8'>
  Appfolio.Listing({
    hostUrl: 'vangorealestate.appfolio.com',
    propertyGroup: 'Park Ave Rutherford Urban Renewal Company LLC',
    themeColor: '#6E6E6E',

    height: '500px',
    width: '100%',
    defaultOrder: 'date_posted'
  });
</script> -->
<!-- <script type='text/javascript' charset='utf-8'>
  document.write(unescape("%3Cscript src='" + (('https:' == document.location.protocol) ? 'https:' : 'http:') + "//vangorealestate.appfolio.com/javascripts/listing.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type='text/javascript' charset='utf-8'>
  Appfolio.Listing({
    hostUrl: 'vangorealestate.appfolio.com',
    propertyGroup: 'Park Ave Rutherford Urban Renewal Company LLC',
    themeColor: '#6E6E6E',
    height: '500px',
    width: '100%',
    defaultOrder: 'date_posted'
  });
</script> -->
<!-- </section> -->

<section>
    <script type='text/javascript' charset='utf-8'>
      document.write(unescape("%3Cscript src='" + (('https:' == document.location.protocol) ? 'https:' : 'http:') + "//vangorealestate.appfolio.com/javascripts/listing.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <div class="location-button-container">
              <button id="button106" class="active location-btn">Parker 106</button>

      <button id="button118" class="location-btn">Parker 118</button>
      </div>

    <div id="118Listings" style="display: none;">
     
    <script type='text/javascript' charset='utf-8'>
      Appfolio.Listing({
        hostUrl: 'vangorealestate.appfolio.com',
        propertyGroup: '118 Park Urban Renewal LLC',
        themeColor: '#6E6E6E',
        height: '500px',
        width: '100%',
        defaultOrder: 'date_posted'
      });
    </script>
    </div>

<div id="106Listings" style="display: block;">
  
    <script type='text/javascript' charset='utf-8'>
  Appfolio.Listing({
    hostUrl: 'vangorealestate.appfolio.com',
    propertyGroup: 'Park Ave Rutherford Urban Renewal Company LLC',
    themeColor: '#6E6E6E',
    height: '500px',
    width: '100%',
    defaultOrder: 'date_posted'
  });
</script>
</div>
  </section>

  <script>
    const button118 = document.getElementById('button118');
    const button106 = document.getElementById('button106');
    const listings118 = document.getElementById('118Listings');
    const listings106 = document.getElementById('106Listings');


    document.addEventListener('DOMContentLoaded', function() {
      button118.addEventListener('click', function() {
        
        listings118.style.display = 'block';
        listings106.style.display = 'none';
        button118.classList.add('active');
        button106.classList.remove('active');
        
      });

      button106.addEventListener('click', function() {
        
        listings118.style.display = 'none';
        listings106.style.display = 'block';
        button118.classList.remove('active');
        button106.classList.add('active');
        
        
      });
    });
  </script>

<!-- Modal -->
<div id="modal" class="modal">
    <div class="modal-content">
        <div class="modal-left">
            <div class="modal-logo">
                <img src="images/logo-white-sm.svg" alt="#">
            </div>
            <div class="floorplan-desc">
                <h3 class="room-title">RESIDENCE 07</h3>
                <div>Floors 1-3</div>
                <div>Studio / 1 Bath</div>
                <div>824 sq. ft.</div>
            </div>
        </div>
        <div class="modal-right">
            <div class="floorplan-img">
                <img src="images/room.jpg" alt="#">
            </div>
        </div>
        <div class="modal-btn">
            <a href="" class="btn btn-primary">Apply</a>
            <a href="" class="btn btn-primary">Download</a>
        </div>
    </div>
</div>

<div id="modal2" class="modal">
    <div class="modal-content">
        <div class="modal-left">
            <div class="modal-logo">
                <img src="images/logo-white-sm.svg" alt="#">
            </div>
            <div class="floorplan-desc">
                <h3 class="room-title">RESIDENCE 07</h3>
                <div>Floors 1-3</div>
                <div>Studio / 1 Bath</div>
                <div>824 sq. ft.</div>
            </div>
        </div>
        <div class="modal-right">
            <div class="floorplan-img">
                <img src="images/room2.jpg" alt="#">
            </div>
        </div>
        <div class="modal-btn">
            <a href="" class="btn btn-primary">Apply</a>
            <a href="" class="btn btn-primary">Download</a>
        </div>
    </div>
</div>

<?php include('_footer.php')?>