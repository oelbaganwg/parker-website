<div class="popup">
	<div class="holdcontent">
        <button class="closex" onclick="toggleModal()"><img src="images/popup/closex.svg" alt="Close Modal"></button>
        <div class="logo"><img src="images/popup/TheParker-118-Logo.svg" alt="118 Park logo"></div>
        <h1>Park Avenue Living</h1> 
        <h2>New Studio, One & Two Bedroom Luxury Rentals <span class="holdtogether">Coming Soon</span></h2>
        <div class="formarea">
            <p class="hideafterform">Get on the list and be the first to hear from our leasing team</p>
            <div class="holdthanks">Thank you for your inquiry</div>
            <div class="holdform">
                <form id="theform1">
                <div class="holdfields">
                    <div class="input">
                        <label for="formdata_name">Full Name*</label>
                        <input type="text" name="formdata_name" class="contactfields" />
                    </div>

                    <div class="input email">
                        <label for="formdata_email">Email*</label>
                        <input type="text" name="formdata_email" class="contactfields" />
                    </div>
                </div>
                <div class="holdfields">
                    <div class="input phone">
                        <label for="formdata_phone">Phone</label>
                        <input type="text" name="formdata_phone" class="contactfields" />
                    </div>
            
                
                    <div class="input">
                        <label for="formdata_comments">Comments</label>
                        <input type="text" name="formdata_comments" class="contactfields" />
                    </div>
                </div>
                <div class="holdfields">
                    <div class="input calendar" onclick="showCalendarInput()">
                        <label for="formdata_movein_date">Move-In Date</label>
                        <input id="calendarinput" type="date" name="formdata_movein_date" class="contactfields" />
                    </div>
            
                
                    <div class="input hometype">
                        <label for="formdata_home_type">Home Type</label>
                        <!-- <input type="text" name="formdata_home_type" class="contactfields" /> -->
                            <select
                                name="formdata_home_type"
                            >
                            <option value=""></option>
                            <option value="studio">Studio</option>
                            <option value="1-bedroom">1 Bedroom</option>
                            <option value="2-bedroom">2 Bedroom</option>
                            </select>
                    </div>
                </div>
              <div class="formerror"></div>
              <div class="holdbtncontainerstatus" style="display:none;">Sending ...</div>
              <div class="holdbtn">
                <button class="btn" onclick="sendForm()" type="button">Submit</button>
              </div>
                    <input type="hidden" name="domainAccountId" value="LAS-337185-55" />
                    <input type="hidden" name="guid" value="" />
                    <input type="hidden" name="formdata_property" value="118 Park" />
                    
                    <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
                    <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
                    <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
                    <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
                </form>
            </div>
        </div>
    </div>
    <div class="vanlogo">
        <img src="images/popup/Vango-Logo.svg" alt="Vango Development">
    </div>
    <footer>
        <div class="content">
            118 park Avenue, Rutherford, NJ 07070 / <a href="tel:201.933.2106">201.933.2106</a> / <a href="mailto:info@renttheparker.com ">Info@RentTheParker.com </a>
            <div class="ehologo">
                <img src="images/popup/EH-Logo.svg" alt="EHO">
            </div>
        </div>
    </footer>
</div>
<div class="popup_overlay"></div>
<script>
    const calendarfield = document.getElementById("calendarfield");
    const calendarinput = document.getElementById("calendarinput");
    function showCalendarInput() {
        calendarinput.style.opacity = 1
    }
</script>