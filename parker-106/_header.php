<!--header-->
<div class="top-block">
    <!--top banner-->
    <div class="top-mess">Ask Our Leasing Team About Current Move‑In Specials!</div>
    <!--end top banner-->

    <header class="header">
        <!-- <div class="mobile-icons">
            <a href="contact" class="icon-mail"></a>
            <a href="tel:201.933.2106" class="icon-phone"></a>
        </div> -->
        
        <div class="header-dropdown">
            <div id="dropdowntext" class="label" onclick="toggleDropdown()">Communities</div>
            <div id="dropdownmenu" class="dropdown">
                <ul>
                    <li class="active"><a href="./parker-106">Parker 106</a></li>
                    <li><a href="../parker-118">Parker 118</a></li>
                </ul>
            </div>
        </div>
        <div class="header-left">
            <div class="logo">
                <a href="<?php echo $domain; ?>"><img src="images/logos_updated/TheParker-106-Black.svg" alt="#"></a>
            </div>
        </div>
        <div class="header-right">
            <nav class="main-nav">
                <ul class="main-menu">
                    <li class="availability_link"><a href="availability">AVAILABILITY</a></li>
                    <li class="contact_link desktopxllink"><a href="contact">CONTACT</a></li>
                    <li><a href="./availability">APPLY</a></li>
                </ul>
            </nav>
            <button class="open-menu-btn">
                 <i class="icon-menu-button"></i>
            </button>
        </div>
    </header>
</div>
<!--end header-->

<!--menu-->
<div class="menu-wrap">
    <div class="menu-inner">
        <div class="menu-inner-block">
            <nav>
                <ul class="mobile-menu">
                    <li class="home_link"><a href="index">Home</a></li>
                    <li class="residences_link"><a href="residences">Residences</a></li>
                    <li class="amenities_link"><a href="amenities">Amenities</a></li>
                    <li class="neighborhood_link"><a href="neighborhood">Neighborhood</a></li>
                    <li class="availability_link"><a href="availability">Availability</a></li>
                    <li class="gallery_link"><a href="gallery">Gallery</a></li>
                    <li class="virtualtour_link"><a href="virtualtour">Virtual Tour</a></li>
                    <li class="contact_link"><a href="contact">Contact</a></li>
                </ul>
            </nav>
            <ul class="mobile-menu-footer">
                <li><a href="./availability">APPLY NOW</a></li>
                <li><a href="https://passport.appf.io/sign_in?idp_type=tportal&vhostless=true" target="_blank">RESIDENT PORTAL</a></li>
            </ul>
            <div class="social_links">
                <a href="https://www.facebook.com/renttheparker/" target="_blank"><img src="images/facebook-logo.svg" alt=""></a>
                <a href="https://www.instagram.com/renttheparker/" target="_blank"><img src="images/instagram-logo.svg" alt=""></a>
            </div>
        </div>
    </div>
</div>
<!--end menu-->
<script>
    function toggleDropdown() {
        document.getElementById("dropdownmenu").classList.toggle("activate");
        document.getElementById("dropdowntext").classList.toggle("activate");
    }
</script>