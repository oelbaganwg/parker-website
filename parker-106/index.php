<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="format-detection" content="telephone=no">
	<title>The Parker Rentals | Apartments For Rent in Rutherford </title>
	<meta name="description" content="Brand New Studio, 1, and 2 Bedroom Rentals in Rutherford. The Parker Apartments are now leasing. Contemporary Designs and Premier Amenities. On-site Parking. Fitness Center. Commuter Friendly Location. Now Leasing. ">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/favicon.png">
	<link rel="stylesheet" href="css/fullpage.css"/>
	<link rel="stylesheet" href="css/fancybox.css"/>
	<link rel="stylesheet" href="css/aos.css"/>
	<link rel="stylesheet" href="css/style.css?v=1.3"/>
    <link rel="stylesheet" href="styles/styles.min.css?v=1.06"/>

	<?php include('header-scripts.php') ?>

</head>
<body class="home">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>
<?php include('_popup.php') ?>

<div id="fullpage">
	<div id="intro" class="section" data-anchor="intro-page">
		<div class="fp-bg">
			<img src="images/TheParker-02-OVERLAY.png" alt="#" class="full-img">
		</div>
		<div class="intro-block">
			<div class="intro-logo">
				<img src="images/logos_updated/TheParker-106-White.svg" alt="#">
			</div>
			<div class="intro-wrap">
				<h1 class="intro-title">DOWNTOWN LIVING<br />PARK AVENUE STYLE</h1>
				<div class="intro-desc">Studio, One & Two Bedroom Luxury Rentals</div>
			</div>
		</div>
		<button class="scroll-down"><i class="icon-angle-down"></i></button>
	</div>
    <div class="section residences" data-anchor="residences">
		<div class="sub-block">
			<h2 class="section-title"><span>RESIDENCES</span></h2>
		</div>
		<div class="fp-bg">
			<img src="images/TheParker10.jpg" alt="The Parker" class="full-img fix">
		</div>
        <div class="section-footer">
			<button class="scroll-down"><i class="icon-angle-down"></i></button>
            <div class="section-footer-block">With an unsurpassed attention to detail and a sleek contemporary vibe, the studio, one, and two bedroom residences at The Parker are changing the way you rent in Rutherford.</div>
            <a href="residences.php" class="btn btn-primary">EXPLORE RESIDENCES</a>
        </div>
    </div>
	<div class="section amenities" data-anchor="amenities">
		<div class="sub-block">
			<h2 class="section-title"><span>AMENITIES</span></h2>
		</div>
		<div class="fp-bg">
			<img src="images/TheParker17a.jpg" alt="#" class="full-img">
		</div>
		<div class="section-footer">
			<button class="scroll-down"><i class="icon-angle-down"></i></button>
			<div class="section-footer-block">We invite you to experience a lifestyle complete with unparalleled resident perks and conveniences designed
				to serve your every whim and need. From a gorgeous private fitness center to the convenience of on-site
				garage parking, life at The Parker delivers on renting like a rock-star!</div>
			<a href="amenities.php" class="btn btn-primary">EXPERIENCE AMENITIES</a>
		</div>
	</div>
	<div class="section neigh" data-anchor="neigh">
		<div class="sub-block">
			<h2 class="section-title"><span>NEIGHBORHOOD</span></h2>
		</div>
		<div class="fp-bg">
			<img src="images/493074473-parker106-home-neighborhood.jpeg" alt="#" class="full-img">
		</div>
		<div class="section-footer">
			<button class="scroll-down"><i class="icon-angle-down"></i></button>
			<div class="section-footer-block">Home to the Meadowlands Sports Complex and only moments from Manhattan by train, Rutherford is quickly
				becoming one of the most desirable neighborhoods for commuters and lifestyle enthusiasts alike. Dining,
				shopping, farmer's markets and community events; this blossoming area is the perfect place to call home.</div>
			<a href="neighborhood.php" class="btn btn-primary">ENGAGE WITH THE NEIGHBORHOOD</a>
		</div>
	</div>
	<div class="section contacts_section" data-anchor="contacts">
        <div class="contacts-block">
            <h2 class="contacts-title">CONTACT US</h2>
            <p>Don't miss this opportunity to be part of The Parker, Rutherford's most highly-anticipated
                residential community.<span class="pleaseregister"> Please register below to learn more, and a member of our leasing team will be in touch as soon as possible.</span></p>
           <!--  <p style="line-height: 1.5">Leasing Office  Hours:<br>
                Monday - Friday: 10 am to 6pm | Saturday & Sunday: 11am to 5pm</p> -->
            <p><a href="https://goo.gl/maps/KFYFayqAA2w" class="under-link" target="_blank">Get Directions.</a></p>
			<p class='holdthanks'>Thank you for your Inquiry.</p>
            <div class="form-block holdform">
                <form id="theform">
	                <div>
                        <input  id="formdata_name" name="formdata_name" type="text" class="form-control" placeholder="NAME*" required>
                    </div>
	                <div>
                        <input id="formdata_email" name="formdata_email"  type="email" class="form-control" placeholder="EMAIL*" required>
                    </div>
                    <div>
                        <input id="formdata_phone" name="formdata_phone" type="tel" class="form-control" placeholder="PHONE">
                    </div>
                    <div class="form-footer">
                        <button class="btn-link formsubmit" type="button">SUBMIT</button>
                    </div>
                    <input id="formdata_comments" name="formdata_comments" type="hidden" value="">
                    <input type="hidden" name="domainAccountId" value="LAS-337185-55" />
                    <input type="hidden" name="guid" value="" />
                    <input type="hidden" name="formdata_property" value="The Parker 106" />
                  <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
                </form>
            </div>
        </div>
        <div class="contacts-footer">
	        <address>106 Park Ave, Rutherford, NJ 07070 | <a href="mailto:Info@RentTheParker.com">Info@RentTheParker.com</a> |
		        <a href="tel:2019332106">201.933.2106</a></address>
            <div class="logos">
                <div class="logos-block">
                    <img src="images/vango.svg" alt="#">
                    <!-- <img src="images/tmd_logo.svg" alt="#"> -->
                    <img src="images/eho_logo.svg" alt="#" style="width: 22px;">
                </div>
                <div class="logos-text">
									All dimensions are approximate and subject to normal construction variances, and tolerances. Plans may contain minor variations from floor to floor.
                </div>
            </div>
        </div>
	</div>
</div>
<script src="//app.lassocrm.com/analytics.js" type="text/javascript"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type='text/javascript' src="js/fullpage.min.js"></script>
<script type='text/javascript' src="js/jquery.nicescroll.min.js"></script>
<script type='text/javascript' src="js/jquery.fancybox.min.js"></script>
<script type='text/javascript' src="js/stupidtable.min.js"></script>
<script type='text/javascript' src="js/imagesloaded.pkgd.min.js"></script>
<script type='text/javascript' src="js/isotope.pkgd.min.js"></script>
<script type='text/javascript' src="js/aos.js"></script>
<script type='text/javascript' src="js/jquery.modal.js"></script>
<script type='text/javascript' src="js/script.js"></script>
<script type='text/javascript' src="scripts/all.min.js"></script>
<script src="//app.lassocrm.com/analytics.js" type="text/javascript"></script>

<script type="text/javascript">
// setTimeout(toggleModal, 2000);
</script>
<script type="text/javascript">
<!--
var LassoCRM = LassoCRM || {};
(function(ns){
    ns.tracker = new LassoAnalytics('LAS-337185-55');
})(LassoCRM);
try {
    LassoCRM.tracker.setTrackingDomain("//app.lassocrm.com");
    LassoCRM.tracker.init();  // initializes the tracker
    LassoCRM.tracker.track(); // track() records the page visit with the current page title, to record multiple visits call repeatedly.
    LassoCRM.tracker.patchRegistrationForms();
} catch(error) {}
-->
</script>
</body>
</html>