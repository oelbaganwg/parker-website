jQuery(document).ready(function ($) {
  setTimeout(function () {
    $(".page_loader").fadeOut(500);
    $("body").addClass("loaded");
  }, 700);

  $(window).resize(function () {
    if ($("#fullpage").length > 0) {
      if ($(window).width() < 768 && $("html").hasClass("fp-enabled")) {
        fullpage_api.destroy("all");
      } else if ($("html").hasClass("fp-enabled")) {
        fullpage_api.reBuild();
      } else if ($(window).width() >= 768) {
        autoHeightMobile();
      }
    }
  });

  function autoHeightMobile() {
    $("#fullpage").fullpage({
      licenseKey: "876FC625-066449C7-B89778D8-B98B79FD",
      responsiveHeight: 600,
      lockAnchors: true,
      scrollingSpeed: 1000,
    });
  }
  if ($(window).width() > 767) {
    autoHeightMobile();
  }

  $("#fullpage .scroll-down").on("click", function () {
    if ($(window).width() > 767) {
      $.fn.fullpage.moveSectionDown();
    } else {
      var thisP = $(this).offset().top;
      $("body, html").animate(
        {
          scrollTop: thisP,
        },
        700
      );
    }
  });

  $(".scroll_on_screen").on("click", function () {
    var thisP = $(this).offset().top;
    $("body, html").animate(
      {
        scrollTop: thisP,
      },
      700
    );
  });

  //MAIN MENU
  $(".open-menu-btn").on("click", function () {
    $("body").toggleClass("opened_menu");
    if ($("body").hasClass("open_menu")) {
      $("body").removeClass("open_menu");
      if ($("#fullpage").length > 0) {
        $.fn.fullpage.setAllowScrolling(true);
        $.fn.fullpage.setKeyboardScrolling(true);
      }
    } else {
      setTimeout(function () {
        $("body").addClass("open_menu");
      }, 1100);
      if ($("#fullpage").length > 0) {
        $.fn.fullpage.setAllowScrolling(false);
        $.fn.fullpage.setKeyboardScrolling(false);
      }
    }
  });

  //Height
  function setHeight() {
    var viewportHeight = $(window).outerHeight();
    $(".primary-section").css({ height: viewportHeight });
  }
  setHeight();
  $(window).on("resize orientationchange", setHeight);

  $(".scroll-block").niceScroll({
    cursorborder: "3",
    cursoropacitymin: 1,
    cursorcolor: "#8b8c8f",
    cursorwidth: 20,
    cursorborderradius: 4,
    background: "#e3e3e5",
    horizrailenabled: false,
    railpadding: {
      top: 30,
      right: 6,
      left: 6,
      bottom: 30,
    },
  });

  var $grid = $(".gallery-wrap").isotope({
    itemSelector: ".gallery-block",
  });
  $grid.imagesLoaded().progress(function () {
    $grid.isotope("layout");
  });
  $grid.isotope({ filter: "*" });

  $(".tab-nav a").on("click", function (e) {
    e.preventDefault();
    $(this)
      .closest(".tab-nav")
      .find(".tabs-item-active")
      .removeClass("tabs-item-active");
    $(this).closest(".tabs-item").addClass("tabs-item-active");
    var filterValue = $(this).data("filter");
    $grid.isotope({ filter: filterValue });

    setTimeout(function () {
      AOS.refresh();
    }, 500);
  });

  //Fancybox
  $("[data-fancybox]").fancybox({
    loop: true,
    keyboard: true,
    toolbar: true,
    animationEffect: "zoom-in-out", //false,zoom,fade, zoom-in-out
    transitionEffect: "slide", //false,fade, slide, circular,tube, zoom-in-out, rotate
    transitionDuration: 600,
    btnTpl: {
      close:
        '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M10,10 L30,30 M30,10 L10,30" />' +
        "</svg>" +
        "</button>",
      // Arrows
      arrowLeft:
        '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M10,20 L30,20 L10,20 L18,28 L10,20 L18,12 L10,20"></path>' +
        "</svg>" +
        "</button>",

      arrowRight:
        '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M30,20 L10,20 L30,20 L22,28 L30,20 L22,12 L30,20"></path>' +
        "</svg>" +
        "</button>",
    },
    // What buttons should appear in the top right corner.
    buttons: [
      //'download',
      //'zoom',
      "close",
    ],
  });

  $(".floorplansnav .tabs-item a").on("click", function () {
    var desiredbeds = $(this).data("id");
    if (desiredbeds == "all") {
      $("table tr").removeClass("remove");
    } else {
      $("table tr").removeClass("remove");
      $("table tbody tr:not(." + desiredbeds + ")").addClass("remove");
    }
  });

  $(".sortable").stupidtable();

  /*MODAL*/
  $("a[data-modal]").click(function (event) {
    $(this).modal({
      fadeDuration: 700,
      fadeDelay: 0.5,
    });
    return false;
  });

  if ($(window).width() < 768) {
    $(".map-block a").on("click", function (e) {
      e.preventDefault();
      var thisP = $("#map").offset().top;
      $("body, html").animate(
        {
          scrollTop: thisP - 80,
        },
        700
      );
    });

    var viewportHeight = $(window).outerHeight();
    $("#intro").css({ height: viewportHeight });
  }

  AOS.init({
    offset: 100,
  });
});

//AJAX SUBMIT FORM
// $('.formsubmit').click(function() {
$(document).on("click", ".formsubmit", function (event) {
  event.preventDefault();
  //check full name
  if (/\w+\s+\w+/.test($("#formdata_name").val())) {
    console.log("good");
  } else {
    console.log("bad");
    $("#formerror").fadeOut();
    $("#formerror").html("Please enter a full name.");
    $("#formerror").fadeIn();
    return false;
  }

  // var moveindate = $('#date_month').val() + '/' + $('#date_day').val() + '/' + $('#date_year').val();
  var url = "action/contact.php";

  //if name is split // and account for fname var in actions/ php
  // var fname = $('#formdata_fname').val();
  // var lname = $('#formdata_lname').val();
  // var grabpostdata = $('#theform').serialize();
  // var postData = grabpostdata + "&formdata_name=" + fname + " " + lname;

  var postData = $("#theform").serialize();
  // var postData = postData_get + '&formdata_move_in_date=' + moveindate;

  $.ajax({
    type: "POST",
    url: url,
    data: postData,
    dataType: "html",
    beforeSend: function () {
      // setting a timeout
      $(".formsubmit").addClass("on");
      $(".formsubmit").html(
        "SENDING <span>.</span><span>.</span><span>.</span>"
      );
      console.log(postData);
    },
    success: function (data) {
      $(".formsubmit").removeClass("on");

      if (data == "Success") {
        // $('#formerror').fadeOut();
        $(".holdform,.pleaseregister").fadeOut(function () {
          $(".holdthanks").fadeIn();
        });

        // fbq('track', 'Lead', {
        // value: 20.00,
        // currency: 'USD'
        // });

        //track Adwords conversion
        gtag("event", "conversion", {
          send_to: "AW-1029912699/Y4hmCK7V-KYBEPvwjOsD",
        });
      } else if (data == "nameerror") {
        $("#formerror").html("Please enter a first and last name");
        $(".formsubmit").html("Contact Us");
      } else {
        $("#formerror").fadeIn();
        $("#formerror").html(data);
        $(".formsubmit").html("SUBMIT");
      }
    },
    // dataType: 'html'
  });
});

console.log("Hello from Script");
