function toggleModal() {
  document.querySelector(".popup_overlay").classList.toggle("active");
  document.querySelector(".popup").classList.toggle("active");
}
//form placeholders
$(".contactfields").on("input", function (e) {
  currentinputid = e.currentTarget;
  parentdiv = $(currentinputid).parent();
  //get the label of the parent of input
  var thelabel = parentdiv.find("label");
  countchar = $(currentinputid).val().length;
  if (countchar > 0) {
    thelabel.fadeOut(0);
  } else {
    thelabel.fadeIn(0);
  }
});

$(".input label").on("click", function (e) {
  currentinputid = e.currentTarget;
  $(currentinputid).fadeOut(0);
  $(currentinputid).next("input").focus();
  countchar = $(currentinputid).val().length;
  if (countchar > 0) {
    currentinputid.fadeIn(0);
  }
});
$("input.contactfields").focusout("input", function (e) {
  currentinputid = e.currentTarget;
  parentdiv = $(currentinputid).parent();
  //get the label of the parent of input
  var thelabel = parentdiv.find("label");
  countchar = $(currentinputid).val().length;
  console.log(countchar);
  if (countchar > 0) {
    thelabel.fadeOut(0);
  } else {
    thelabel.fadeIn(0);
  }
});
//ajax converted to javascript function
async function sendForm() {
  const form = document.querySelector("form");
  const errorElem = document.querySelector(".formerror");
  const holdFormElem = document.querySelector(".holdform");
  const holdThanksElem = document.querySelector(".holdthanks");
  const hideafterform = document.querySelector(".hideafterform");
  const holdBtnElem = document.querySelector(".holdbtn");
  const holdBtnStatusElem = document.querySelector(".holdbtncontainerstatus");
  holdBtnElem.style.display = "none";
  holdBtnStatusElem.style.display = "block";

  let formDataValues = await new FormData();
  const allFormValues = Object.values(form).reduce((obj, field) => {
    obj[field.name] = field.value;
    formDataValues.append([field.name], field.value);
    return obj;
  }, {});
  const postObj = {
    method: "POST",
    body: formDataValues,
  };
  const url = "action/contact.php";
  const response = await fetch(url, postObj);
  const responseJson = await response.text();
  if (responseJson === "Success") {
    console.log("Success");
    holdFormElem.style.display = "none";
    hideafterform.style.display = "none";
    holdThanksElem.style.display = "block";
    holdBtnStatusElem.style.display = "none";
  } else {
    errorElem.innerText = responseJson;
    errorElem.style.display = "block";
    holdBtnElem.style.display = "block";
    holdBtnStatusElem.style.display = "none";
  }
  console.log(responseJson);
}

console.log("Hello from Main");
