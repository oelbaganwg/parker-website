<?php 
if ( isset($_GET["utm_source"]) && ($_GET["utm_source"] != "")) {
    setcookie("utm_source", $_GET["utm_source"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
  }
  if ( isset($_GET["utm_campaign"]) && $_GET["utm_campaign"] != "") {
    setcookie("utm_campaign", $_GET["utm_campaign"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
  }
  if ( isset($_GET["utm_medium"]) && $_GET["utm_medium"] != "") {
    setcookie("utm_medium", $_GET["utm_medium"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
  }
  if ( isset($_GET["utm_term"]) && $_GET["utm_term"] != "") {
    setcookie("utm_term", $_GET["utm_term"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
  }
   ?><!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '476540372913094');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=476540372913094&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PHEVC1073G"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-X6L2QJSK8X"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-PHEVC1073G'); //master
      gtag('config', 'G-X6L2QJSK8X'); //parker 106
      // gtag('config', 'AW-1029912699');
      // gtag('event', 'page_view', {
      //   'send_to': 'AW-1029912699'
      // });
    </script>