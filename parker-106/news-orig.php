<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Rutherford NJ | New Luxury Rentals in NJ</title>
    <meta name="description" content="New Rentals in Rutherford, NJ. Spacious studio, 1, & 2 bedroom rentals at The Parker. VIP Amenities and Commuter Friendly Location. Now Leasing. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="news">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/News-1.jpg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">READ ALL ABOUT IT</h1>
        <p>News & Press</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- news-section -->
<section class="gallery-section news-section">
    <div class="container">
        <div class="tab-container">
            <ul id="filters" class="tab-nav" data-aos="fade-up">
                <li class="tabs-item tabs-item-active"><a href="#" data-id="all" data-filter="*">All</a></li>
                <li class="tabs-item"><a href="#" data-id="residences" data-filter=".news">NEWS</a></li>
                <li class="tabs-item"><a href="#" data-id="amenities" data-filter=".blog">BLOG</a></li>
            </ul>
            <div class="tab_content">
                <div class="tab-content-item">
                    <div class="gallery-wrap">
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-2.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Leasing Begins At The Parker, 52-Unit Rental At 106 Park Avenue In Rutherford</a></h3>
                                <div class="news-date">September 01, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-3.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Renderings Revealed For The Parker, Rutherford’s Newest Rental Building</a></h3>
                                <div class="news-date">August 08, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 blog">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-4.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">The Parker’s Luxury Rentals On Rutherford’s Park Ave To Debut This Fall</a></h3>
                                <div class="news-date">July 20, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-2.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Leasing Begins At The Parker, 52-Unit Rental At 106 Park Avenue In Rutherford</a></h3>
                                <div class="news-date">September 01, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-3.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Renderings Revealed For The Parker, Rutherford’s Newest Rental Building</a></h3>
                                <div class="news-date">August 08, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-4.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">The Parker’s Luxury Rentals On Rutherford’s Park Ave To Debut This Fall</a></h3>
                                <div class="news-date">July 20, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 blog">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-2.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Leasing Begins At The Parker, 52-Unit Rental At 106 Park Avenue In Rutherford</a></h3>
                                <div class="news-date">September 01, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 news">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-3.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">Renderings Revealed For The Parker, Rutherford’s Newest Rental Building</a></h3>
                                <div class="news-date">August 08, 2019</div>
                            </div>
                        </div>
                        <div class="gallery-block gallery-block-width2 blog">
                            <div data-aos="fade-up">
                                <a href="#"><img src="images/News-4.jpg" alt="#"></a>
                                <h3 class="news-title"><a href="#">The Parker’s Luxury Rentals On Rutherford’s Park Ave To Debut This Fall</a></h3>
                                <div class="news-date">July 20, 2019</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('_footer.php')?>