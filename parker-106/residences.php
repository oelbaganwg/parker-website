<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Residences | Brand New Rentals in Rutherford NJ</title>
    <meta name="description" content="Studio to 2-bedroom rentals near train. Kohler Fixtures. Quartz Countertops and Stainless Steel Appliances. Spacious Custom Closets and Plank Wood Flooring. Now Leasing. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="residences">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/TheParker-08-2.jpg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">POSH LIFE</h1>
        <p>The Residences</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- info-section -->
<section class="info-section">

    <div class="col-46 order-md-2">
        <div class="container">
            <div class="info-img" data-aos="fade-up">
                <img src="images/TheParker 12.jpg" alt="#">
            </div>
            <div class="info-body" data-aos="fade-up">
                <h3 class="block-title">WHERE STYLE MEETS COMFORT</h3>
                <p>Host an evening soirée in your chef-inspired gourmet kitchen featuring stainless steel appliances, white lacquer cabinetry and Quartz countertops. Unwind in your spa-like bathroom boasting stunning vanities and Quartz countertops or take in the view of Manhattan right outside your window. Welcome to a premier lifestyle experience, a place where contemporary style meets all the comforts of a perfect home.</p>
            </div>
            <div data-aos="fade-up">
                <h4 class="sub-title">RESIDENCE FEATURES:</h4>
                <ul>
                    <li>Gourmet Kitchens Featuring White Lacquer Cabinetry, Quartz Countertops, Kohler Fixtures and Stainless Steel Appliances</li>
                    <li>Spa-like Master Baths with Custom Vanities and Quartz Countertops</li>
                    <li>Full Size In-Home Washer and Dryer</li>
                    <li>Luxury Plank Flooring</li>
                    <li>Energy-Efficient Central Heat and Air</li>
                    <li>Oversized Operable Windows</li>
                    <li>High Ceilings</li>
                    <li>Spacious Custom Closets</li>
                    <li>Pre-Wired for FIOS</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-54">
        <div class="img-block" data-aos="fade">
            <img src="images/TheParker 09.jpg" alt="#" class="full-img">
        </div>
    </div>
</section>

<!-- sub-section -->
<section class="sub-section" data-aos="fade">
    <img src="images/TheParker-13s-EDIT-3.jpg" alt="#" class="full-img">
</section>

<?php include('_footer.php')?>