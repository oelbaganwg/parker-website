<?php 
	define('ROOTPATH', realpath(dirname(__FILE__)) . '/');
	// installed in the docroot?
	if (realpath(dirname(__FILE__)) == $_SERVER['DOCUMENT_ROOT'])
	{
	    define('ROOT', '/');
	}
	else
	{
	    define('ROOT', substr(ROOTPATH, strlen($_SERVER['DOCUMENT_ROOT'])+1));
	}

	//live
	$config['base_url'] = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/" . ROOT;
	$domain = $config['base_url'];

	if ( isset($_GET["utm_source"]) && ($_GET["utm_source"] != "")) {
		setcookie("utm_source", $_GET["utm_source"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_campaign"]) && $_GET["utm_campaign"] != "") {
		setcookie("utm_campaign", $_GET["utm_campaign"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_medium"]) && $_GET["utm_medium"] != "") {
		setcookie("utm_medium", $_GET["utm_medium"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_term"]) && $_GET["utm_term"] != "") {
		setcookie("utm_term", $_GET["utm_term"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}