<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>Rutherford Apartment Rentals | The Parker</title>
      <meta name="description" content="Studio to 2-bedroom rentals in NJ with premier amenities. Fitness Center. Resident Lounge. Club Room. On-site Parking.  Now Leasing. Contact us today.">
      <?php include('header-scripts.php'); ?>  
      <style>
        .swiper-container {
          width: 100%;
          height: 100%;
          margin-left: auto;
          margin-right: auto;
        }
        .swiper-pagination-bullet-active {
          background:#fff;
        }
      </style>
      <!-- Facebook Pixel Code -->
          <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '476540372913094');
            fbq('track', 'PageView');
          </script>
          <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=476540372913094&ev=PageView&noscript=1"
          /></noscript>
          <!-- End Facebook Pixel Code -->
          <!-- Global site tag (gtag.js) - Google Analytics -->
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141511279-1"></script>
          <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-141511279-1');
            gtag('config', 'UA-141511279-2');
            gtag('config', 'AW-1029912699');
            gtag('event', 'page_view', {
              'send_to': 'AW-1029912699'
            });
          </script>

    </head>
    <body class="homepage">

      <header>
        
      
   <!-- 
      <video preload="auto" id="videobg" poster="" class="video-background" loop="" muted="" playsinline="">
         <source src="videos/video.mp4" type="video/mp4">
      </video> -->
       <!-- <div class="img bgimage" style="background-image:url('images/x.jpg');"></div> -->



        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <!-- <div class="swiper-slide bgimage waitforthisimg" style="background-image:url('images/x.jpg');"></div> -->
            <div class="swiper-slide">
              <figure>
                <picture>
                  <source srcset="images/TheParker 02alt.jpg" media="(min-width: 641px)">
                  <source srcset="images/TheParker 02alt.jpg" media="(max-width: 640px)">
                  <img style="background-image:url('images/TheParker 02alt.jpg">
                </picture>
              </figure>
            </div>
          </div>
          <!-- Add Pagination -->
          <!-- <div class="swiper-pagination"></div> -->
          <!-- Add Arrows -->
          <!-- <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div> -->
          <!-- <div class="next navlinks">
            <img src="images/icons/arrow_right.png">
          </div>
          <div class="prev navlinks">
            <img src="images/icons/arrow_left.png">
          </div> -->
        </div>

       <!--  <div class="next navlinks">
          <img src="images/icons/arrow_right.png">
        </div>
        <div class="prev navlinks">
          <img src="images/icons/arrow_left.png">
        </div> -->

        <div class="logo">
          <img src="images/logos_updated/TheParker-106-White.svg" alt="">
        </div>
        
        <div class="holdcontent">
          <div class="copy">
            <h1>New Luxury Rentals In Rutherford</h1>
            <h2>Downtown Living, Park Ave Style. Now Offering One Month Free*</h2>
          </div>

          <div class="contactform">

            <div class="holdthanks">
              <div>Thank you for your inquiry.</div>
              <a href="https://renttheparker.com/?utm_source=Google_Ads&utm_medium=Filled-Form&utm_campaign=Ad_Landing" class="btn">Enter Website</a>
            </div>
            <form id="theform" class="holdform">
              <div class="input">
                <label for="formdata_name">Name</label>
                <input type="text" id="formdata_name" name="formdata_name" class="contactfields" />
              </div>

                <div class="input email">
                  <label for="formdata_email">Email*</label>
                  <input type="text" id="formdata_email" name="formdata_email" class="contactfields" />
                </div>

                <div class="input phone">
                  <label for="formdata_phone">Phone</label>
                  <input type="text" id="formdata_phone" name="formdata_phone" class="contactfields" />
                </div>

           
              <div class="input">
                <label for="formdata_comments">Comments</label>
                <input type="text" id="formdata_comments" name="formdata_comments" class="contactfields" />
              </div>
              <div id="formerror"></div>
              <div class="holdbtn">
                <div class="col">
                  <a href="javascript:;" class="cta btn formsubmit">Register Now</a>
                </div>
                <div class="col entersite">
                  <a href="https://renttheparker.com/?utm_source=Google_Ads&utm_medium=Skipped-Form&utm_campaign=Ad_Landing">Enter Website</a>
                </div>
              </div>
              <input type="hidden" name="domainAccountId" value="LAS-337185-55" />
              <input type="hidden" name="guid" value="" />
              <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
               <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
               <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
               <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
            </form>
          </div>
        </div>



        <div class="bggrad"></div>
      </header>
      

      <script>
        var waitforthisimage = "images/x.jpg";
      </script>
      
      <?php include('footer-scripts.php'); ?>
      <script>
    
        msieversion();
        function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
            {
                // alert('IE');
                $('figure img').attr('src', '');
            }
            else  // If another browser, return 0
            {
                // alert('otherbrowser');
            }

            return false;
        }
        
        var readyclass = 'page_ready';

        if ($('header video').length) { 

          // Start Is Video Ready
            var thevideo = document.getElementById("videobg");
            var videoid = 'videobg'; 
            $(function() {
                var onVideoReady = function(){
                  setTimeout(function(){
                    loadhero();
                      // playvideo(thevideo);
                   }, 0); 
                }
                var onVideoError = function(){
                    loadhero();
                    console.warn('Video failed to load');
                      // playvideo(thevideo);
                }
                var isVideoReady = new videoReady(videoid, onVideoReady, onVideoError);
                    isVideoReady.check();
            });
          // End Is Video Ready

        } else {

          var windowwidth = $( window ).width();
          if (windowwidth < 768) {
            loadhero();
          } else {
          //Start Is Image loaded
            var img = new Image();
            img.onload = function() {
                loadhero();
            }
            img.onerror = function() {
                loadhero();
            };
            img.src = waitforthisimage;
          // End Is Image loaded
          }

        }
        

        function playvideo(thevideo) {
          thevideo.play();
        }

        function loadhero() {

          $('body').addClass(readyclass);
          $('header .coverup').fadeOut(1000);
          $('header .img').fadeIn(1000);
          if ($('header video').length) { 
            playvideo(thevideo);
          }
          var theease = Expo.easeOut;

          var tl = new TimelineMax();
            tl.addLabel('line')
            tl.to('header .copy .line1.hidden .line', 1, {
              top:0,
              opacity:1,
              ease: theease
            },'line+=1.3')
            tl.to('header .arrowdown', 0.2, {
              opacity:1
            })
        }

    
      </script>
    
    </body>
  </html>