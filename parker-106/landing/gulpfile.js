'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var tasknamechange = 'Update Styles';
var tasknamewatch = 'watch';
var srcfile = 'scss/styles.scss';
var outputfolder = 'css';
var watchfolder = 'scss/*.scss';

//compile
 gulp.task(tasknamechange, function () {
 // gulp.src('scss/*.scss') //write multiple files
 gulp.src(srcfile) //write one file
 // .pipe(sass().on('error', sass.logError))
 .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
 // minify
 .pipe(gulp.dest(outputfolder))
 .pipe(browserSync.stream());
 });

 //Watch task
gulp.task(tasknamewatch,function() {
	var files = [
	      '*.php'
	   ];
	browserSync.init(files,{
	    proxy: "localhost/parker-website/landing"
	});
    gulp.watch(watchfolder).on('change', gulp.series(tasknamechange));
});

// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: "localhost/walkerhouse"
//     });
// });


// INSTALL GULP

// 1. install gulp globally
// npm install gulp -g

//2. Install gulp into local directory
// npm init
// npm install gulp --save-dev

//3. Install gulp tasks
// npm install gulp-sass --save-dev

//4. Install Browser Sync
// npm install browser-sync gulp --save-dev

//5. Watch in local directory
// gulp watch

//https://medium.freecodecamp.org/how-to-set-up-gulp-sass-using-the-command-line-if-youre-a-beginner-17729f53249