      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
      <link rel="icon" href="favicon.png">
      <link rel="stylesheet" href="<?php echo $domain; ?>css/styles.css">
      <link rel="stylesheet" href="<?php echo $domain; ?>swiper/css/swiper.min.css">