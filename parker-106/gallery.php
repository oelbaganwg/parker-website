<?php include('domain.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>Apartments for Rent in Rutherford | Amenity-Rich Residential Community</title>
    <meta name="description" content="Studio to 2-bedroom apartments with premier amenities. State-of-the-art Fitness Center. Club Room. On-site Parking. WiFi in Common Areas. Bike Storage. Pet-Friendly. Smoke-Free Community. Now Leasing. ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css?v1.0"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="gallery">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/TheParker_22.jpg" alt="#" class="full-img-oe amenities">
    <div class="container">
        <h1 class="primary-title">SEE IT ALL</h1>
        <p>Gallery</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- gallery-section -->
<section class="gallery-section">
    <div class="container">
        <div class="tab-container">
            <ul id="filters" class="tab-nav" data-aos="fade-up">
                <li class="tabs-item tabs-item-active"><a href="#" data-id="all" data-filter="*">All</a></li>
                <li class="tabs-item"><a href="#" data-id="residences" data-filter=".residences">Residences</a></li>
                <li class="tabs-item"><a href="#" data-id="amenities" data-filter=".amenities">Amenities</a></li>
                <li class="tabs-item"><a href="#" data-id="neighborhood" data-filter=".neighborhood">Neighborhood</a></li>
            </ul>
            <div class="tab_content">
                <div class="tab-content-item">
                    <div class="gallery-wrap">
                        <div class="gallery-block residences">
                            <a href="images/TheParker08.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker08.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block residences">
                            <a href="images/TheParker12.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker12.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block residences">
                            <a href="images/TheParker09.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker09.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block residences">
                            <a href="images/TheParker11.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker11.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block residences">
                            <a href="images/TheParker10.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker10.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block residences">
                            <a href="images/TheParker13s-EDIT2.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker13s-EDIT2.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker01alt.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker01alt.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker05.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker05.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker03s.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker03s.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker06.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker06.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker04s-EDIT.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker04s-EDIT.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker17a.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker17a.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_15.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_15.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_16.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_16.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_18s.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_18s.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_19.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_19.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_20s.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_20s.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_21.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_21.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_22.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_22.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block amenities">
                            <a href="images/TheParker_23.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/TheParker_23.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block neighborhood">
                            <a href="images/Gallery-8B.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery-8A.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block neighborhood">
                            <a href="images/Gallery-9B.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/gallery-9A.jpg" alt="#"></a>
                        </div>
                        <!-- <div class="gallery-block neighborhood">
                            <a href="images/20190712_109ParkAveRutherford-2601.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/20190712_109ParkAveRutherford-2601.jpg" alt="#"></a>
                        </div> -->
                        <div class="gallery-block neighborhood">
                            <a href="images/20190712_109ParkAveRutherford-2528.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/20190712_109ParkAveRutherford-2528.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block neighborhood">
                            <a href="images/20190712_109ParkAveRutherford-2697.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/20190712_109ParkAveRutherford-2697.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block neighborhood">
                            <a href="images/20190712_109ParkAveRutherford-2746-EDIT.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/20190712_109ParkAveRutherford-2746-EDIT.jpg" alt="#"></a>
                        </div>
                        <div class="gallery-block neighborhood">
                            <a href="images/20190712_109ParkAveRutherford-2755.jpg" data-fancybox="gallery" data-aos="fade-up"><img src="images/20190712_109ParkAveRutherford-2755.jpg" alt="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('_footer.php')?>