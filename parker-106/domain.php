<?php 
	define('ROOTPATH', realpath(dirname(__FILE__)) . '/');
	// installed in the docroot?
	if (realpath(dirname(__FILE__)) == $_SERVER['DOCUMENT_ROOT'])
	{
	    define('ROOT', '/');
	}
	else
	{
	    define('ROOT', substr(ROOTPATH, strlen($_SERVER['DOCUMENT_ROOT'])+1));
	}

	if(preg_match("/[a-z]/i", ROOT)){ 
		//LOCAL // if contains letter must be local
		$config['base_url'] = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/" . ROOT;
	} else {
		//LIVE // if does not contain letter must be root live /
		$config['base_url'] = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . ROOT;
	}
	$domain = $config['base_url'];

	if ( isset($_GET["utm_source"]) && ($_GET["utm_source"] != "")) {
		setcookie("utm_source", $_GET["utm_source"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_campaign"]) && $_GET["utm_campaign"] != "") {
		setcookie("utm_campaign", $_GET["utm_campaign"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_medium"]) && $_GET["utm_medium"] != "") {
		setcookie("utm_medium", $_GET["utm_medium"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}
	if ( isset($_GET["utm_term"]) && $_GET["utm_term"] != "") {
		setcookie("utm_term", $_GET["utm_term"], time() + (10 * 365 * 24 * 60 * 60), "/"); 
	}

    // $clientwebsite = "Parker";
    // $key = "c1d1b62c5e180a2a697f4110bd811c";
    // $apiResource = "clientcollection";
    // $apiReferrer = "https://nwgclientcontent.com";


    // //option 1
    // $arrContextOptions=array(
    //   "ssl"=>array(
    //       "verify_peer"=>false,
    //       "verify_peer_name"=>false,
    //   ),
  	// );  
    // $response = file_get_contents($apiReferrer . "/admin/api/collections/get/" . $apiResource . "?token=" . $key, false, stream_context_create($arrContextOptions));
  	// $collectionEntries = json_decode($response);

  	//option 2
  // 	$ch = curl_init();
	 //  curl_setopt ($ch, CURLOPT_URL,  $apiReferrer . "/admin/api/collections/get/" . $apiResource . "?token=" . $key);
	 //  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	 //  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	 //  $file_contents = curl_exec($ch);
	 //  curl_close($ch);
	 //  $collectionEntries = json_decode($file_contents);



    // $incentiveCollection = $collectionEntries->entries;
    // foreach ($incentiveCollection as $incentiveItem) { 
    //   if ($incentiveItem->client == $clientwebsite) {
    //     $incentive = $incentiveItem->incentive;
    //     $disclaimer = $incentiveItem->disclaimer;
    //     $phone = $incentiveItem->phone;
    //     $applynowlink = $incentiveItem->applynowlink;
    //   }
    // }