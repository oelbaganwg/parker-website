      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
      <link rel="icon" href="<?php echo $domain; ?>parker-favicon.png">
      <link rel="stylesheet" href="<?php echo $domain; ?>css/styles.css?v1.1">
      <link rel="stylesheet" href="<?php echo $domain; ?>css/animate.css">
      <link rel="stylesheet" href="<?php echo $domain; ?>css/hover.css">
      <link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>fancybox/jquery.fancybox.min.css">
      <link rel="stylesheet" href="<?php echo $domain; ?>swiper/css/swiper.css">
      <style>
        .fancybox-button,
        .fancybox-navigation button:before {
          background-color:transparent;
        }
      </style>
      <!-- Facebook Pixel Code -->
      <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '476540372913094');
        fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=476540372913094&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141511279-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-141511279-1');
        gtag('config', 'UA-141511279-2');
        gtag('config', 'AW-1029912699');
        gtag('event', 'page_view', {
          'send_to': 'AW-1029912699'
        });
      </script>

    