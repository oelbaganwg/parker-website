    <script src="<?php echo $domain; ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $domain; ?>swiper/js/swiper.min.js"></script>
    <script src="<?php echo $domain; ?>js/jquery.mask.js"></script>
    <script src="<?php echo $domain; ?>js/wow.min.js"></script>
    <script src="<?php echo $domain; ?>fancybox/jquery.fancybox.min.js"></script>
    <script src="<?php echo $domain; ?>js/TweenMax.min.js"></script>
    <script src="<?php echo $domain; ?>js/ScrollMagic.min.js"></script>
    <script src="<?php echo $domain; ?>js/animation.gsap.js"></script>
    <script src="<?php echo $domain; ?>js/debug.addIndicators.min.js"></script>    
    <script src="<?php echo $domain; ?>js/in-view.min.js"></script>
    <script src="<?php echo $domain; ?>js/videoReady.js"></script>
    <script src="<?php echo $domain; ?>js/main.js"></script>
    <script>
        new WOW().init();
    </script>