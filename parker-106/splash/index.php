<?php include('domain.php'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title>The Parker | New Luxury Rentals in Rutherford, NJ </title>
      <meta name="description" content="The Parker is the most highly-anticipated residential community in Rutherford, NJ. Offering Studio, One & Two Bedroom luxury rentals delivering an amenity-rich lifestyle. Coming soon.">
      <?php include('header-scripts.php'); ?>  
      <style>


      </style>

    </head>
    <body class="homepage">

      
      <header>
        <div class="img bgimage" style="background-image:url('images/parker-hero.jpg');"></div> 
        <div class="copy">
          <div class="logo">
            <img src="images/parker-logo_parker-logo.svg" alt="The Parker Logo">
          </div>
          <h1>Studio, One & Two Bedroom Luxury Rentals
            <span class='italic'>Coming Soon</span></h1>
        </div>
        <div class="scrolldown">
          <a href="#main" aria-label="Click to scroll down"><img src="images/icons/scroll-down.svg" alt="Scroll Down image"></a>
        </div>
      </header>

      <main id="main">
        
        <section class="top">
          
          <div class="holdcontent">
            <h2 class="wow fadeIn">Downtown Living</h2>
            <h2 class="wow fadeIn">Park Avenue Style</h2>
            <p class="wow fadeIn">
              Welcome to a residential experience that will change the game in Rutherford forever. With an unsurpassed attention to detail and a sleek contemporary vibe, these studio, one, and two bedroom rental residences are primed to deliver the modern and amenity-rich living you've only dreamed of...in the center of it all.
            </p>
          </div>

        </section>



        <section class="splitbox">
          <div class="holdcontent">
            <div class="col copyarea bgimage" style="background-image:url('images/res-features_BG.jpg');">
              <div class="copy">
                <h2 class="wow fadeInDown">Residence Features</h2>
                <div class="line wow fadeIn"></div>
                <ul>
                  <li class="wow fadeInDown">Oversized Windows</li>
                  <li class="wow fadeInDown">Plank Flooring Throughout</li>
                  <li class="wow fadeInDown">Gourmet Kitchens</li>
                  <li class="wow fadeInDown">Spa-Like Master Baths</li>
                  <li class="wow fadeInDown">High Ceilings</li>
                  <li class="wow fadeInDown">Spacious Custom Closets</li>
                  <li class="wow fadeInDown">Recessed Lighting</li>
                  <li class="wow fadeInDown">Washer/Dryer In Every Residence</li>
                  <li class="wow fadeInDown">Energy-Efficient Central Heat and AC</li>
                  <li class="wow fadeInDown">Pre-Wired for Fios</li>
                </ul>
              </div>
            </div>
            <div class="col photo">

              <!-- Swiper -->
              <div class="swiper-container residences wow fadeIn">
                <div class="swiper-wrapper">
                  <div class="swiper-slide bgimage" style="background-image:url('images/res-features_1.jpg');"></div>
                  <div class="swiper-slide bgimage" style="background-image:url('images/res-features_2.jpg');"></div>
                  <div class="swiper-slide bgimage" style="background-image:url('images/res-features_3.jpg');"></div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="next navlinks">
                  <img src="images/icons/arrow_right.png">
                </div>
                <div class="prev navlinks">
                  <img src="images/icons/arrow_left.png">
                </div>
              </div>

            </div>
          </div>
        </section>


        <section class="splitbox amenities left">
          <div class="holdcontent">
            <div class="col copyarea bgimage" style="background-image:url('images/amenities_BG.jpg');">
              <div class="copy">
                <h2 class="wow fadeInDown">Featured Amenities</h2>
                <div class="lin wow fadeIn"></div>
                <ul>
                  <li class="wow fadeInDown">Steps to the Train</li>
                  <li class="wow fadeInDown">State-of-the-art Fitness Center</li>
                  <li class="wow fadeInDown">Resident's Lounge</li>
                  <li class="wow fadeInDown">Butterfly Intercom</li>
                  <li class="wow fadeInDown">Package Concierge</li>             
                  <li class="wow fadeInDown">On-Site Parking</li>
                  <li class="wow fadeInDown">Pet-friendly</li>     

                </ul>
              </div>
            </div>
            <div class="col photo">

              <!-- Swiper -->
              <div class="swiper-container amenities wow fadeIn">
                <div class="swiper-wrapper">
                  <div class="swiper-slide bgimage" style="background-image:url('images/amenities_1.jpg');"></div>
                  <div class="swiper-slide bgimage" style="background-image:url('images/amenities_2.jpg');"></div>
                  <div class="swiper-slide bgimage" style="background-image:url('images/amenities_4.jpg');"></div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="next navlinks">
                  <img src="images/icons/arrow_right.png">
                </div>
                <div class="prev navlinks">
                  <img src="images/icons/arrow_left.png">
                </div>
              </div>

            </div>
          </div>
        </section>




        <section id="contact" class="splitbox contact">
          <div class="holdcontent">
            <div class="col copyarea bgimage" >
              <div class="copy">
                <h2 class="wow fadeIn">Contact Us</h2>
                <p class="wow fadeIn">Don't miss this opportunity to be part of The Parker, Rutherford's most highly-anticipated residential community. We invite you to join our priority list to be kept up to speed as we lead up to our grand opening this fall.</p>
                <p class="wow fadeIn"><a href="https://goo.gl/maps/KFYFayqAA2w" target="_blank">Get Directions.</a></p>
                
                <div class="holdthanks">
                  Thank you for your inquiry.
                </div>
                <form id="theform"  class="wow fadeIn holdform">
                  <div class="input">
                    <label for="formdata_name">Name*</label>
                    <input type="text" id="formdata_name" name="formdata_name" class="contactfields" />
                  </div>
                  <div class="input">
                    <label for="formdata_email">Email*</label>
                    <input type="text" id="formdata_email" name="formdata_email" class="contactfields" />
                  </div>

                  <div class="input">
                    <label for="formdata_phone">Phone*</label>
                    <input type="text" id="formdata_phone" name="formdata_phone" class="contactfields" />
                  </div>
                 <!--  <div class="input">
                    <label for="formdata_comments">Comments</label>
                    <input type="text" id="formdata_comments" name="formdata_comments" class="contactfields" />
                  </div> -->
                  <div id="formerror"></div>
                  <div class="holdbtn">
                    <a href="javascript:;" class="cta btn formsubmit">Submit</a>
                    <div class="clearfloats"></div>
                  </div>
                  <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
                   <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
                </form>
              </div>
            </div>
            <div class="col photo" >
                <div class="img bgimage" style="background-image:url('images/TheParker_map.jpg');"></div>
              <!-- map -->

            </div>
          </div>
        </section>




      </main>

      <footer>
        <div class="holdcontent">
          <ul>
            <li>106 Park Ave. Rutherford, NJ 07070</li>
            <li><a href="mailto:Info@RentTheParker.com">Info@RentTheParker.com</a></li>
            <li><a href="tel:2019332106">201.933.2106</a></li>
          </ul>
          <div class="footerlogos">
              <!-- <img src="images/tmd_logo.svg" alt="The Marketing Directors Logo"> -->
              <img src="images/eho_logo.svg" alt="Equal Opportunity Housing Logo">
            <div class="disclaimer">
              All images are a combination of photography and artist renderings. The artists representation, interior decorations, finishes, appliances and furnishings are provided for illustrative purposes only.
            </div>
          </div>
        </div>
      </footer>

      
   
    <!--  <video preload="auto" id="videobg" poster="" class="video-background" loop="" muted="" playsinline="">
       <source src="videos/vermellaNJ_header_v3.mp4" type="video/mp4">
     </video> -->
     <!-- <div class="img bgimage" style="background-image:url('images/x.jpg');"></div> -->


      <!-- sample fancybox popup -->
      <!-- <a href="../images/Hoboken_Gallery_1.jpg" class="box bgimage" data-fancybox style="background-image:url('../images/Hoboken_Gallery_1.jpg');"></a> -->

    

      <!-- <form id="theform">
        <div class="input">
          <label for="formdata_name">Name</label>
          <input type="text" id="formdata_name" name="formdata_name" class="contactfields" />
        </div>
        <div class="input">
          <label for="formdata_email">Email</label>
          <input type="text" id="formdata_email" name="formdata_email" class="contactfields" />
        </div>

        <div class="input">
          <label for="formdata_phone">Phone</label>
          <input type="text" id="formdata_phone" name="formdata_phone" class="contactfields" />
        </div>
        
        <div class="holdradiobuttons">
          <div class="label">Bedrooms*</div>
          <label class="radiocontainer">Studio
            <input type="radio" checked="checked" name="formdata_bedrooms" value="Studio">
            <span class="checkmark"></span>
          </label>
          <label class="radiocontainer">1 Bedroom
            <input type="radio" name="formdata_bedrooms"  value="1 Bedroom">
            <span class="checkmark"></span>
          </label>
          <label class="radiocontainer">2 Bedroom
            <input type="radio" name="formdata_bedrooms"  value="2 Bedroom">
            <span class="checkmark"></span>
          </label>
        </div>

        <div class="input">
          <div class="holddate">
            <div class="label">Desired Move-in Date</div>
            <input type="text" id="date_month" name="date_month" maxlength="2" placeholder=" mm" class="movenext" />/
            <input type="text" id="date_day" name="date_day" maxlength="2"  placeholder=" dd" class="movenext" />/
            <input type="text" id="date_year" name="date_year" maxlength="4"  placeholder=" yyyy"  class="year" />
          </div>
        </div>

        <div class="input">
          <label for="formdata_comments">Comments</label>
          <textarea name="formdata_comments" id="formdata_comments" class="contactfields"></textarea>
        </div>
        <div class="holdbtn">
          <a href="javascript:;" class="cta btn">Submit</a>
        </div>
        <input type="hidden" value="<?php if ( isset($_COOKIE["utm_campaign"])) { echo $_COOKIE["utm_campaign"]; } else { if ( isset($_GET["utm_campaign"])) { echo $_GET["utm_campaign"]; } }  ?>" id="formdata_campaign"  name="formdata_campaign" />
         <input type="hidden" value="<?php if ( isset($_COOKIE["utm_medium"])) { echo $_COOKIE["utm_medium"]; }  else { if ( isset($_GET["utm_medium"])) { echo $_GET["utm_medium"]; } }  ?>" id="formdata_medium"  name="formdata_medium" />
         <input type="hidden" value="<?php if ( isset($_COOKIE["utm_source"])) { echo $_COOKIE["utm_source"]; } else { if ( isset($_GET["utm_source"])) { echo $_GET["utm_source"]; } }  ?>" id="formdata_source" name="formdata_source" />
         <input type="hidden" value="<?php if ( isset($_COOKIE["utm_term"])) { echo $_COOKIE["utm_term"]; } else { if ( isset($_GET["utm_term"])) { echo $_GET["utm_term"]; } }  ?>" id="formdata_keywords" name="formdata_keywords" />
      </form> -->

      <script>
        var waitforthisimage = "images/parker-hero.jpg";
      </script>
      
      <?php include('footer-scripts.php'); ?>
      <script>
    
        
        var readyclass = 'page_ready';

        if ($('header video').length) { 

          // Start Is Video Ready
            var thevideo = document.getElementById("videobg");
            var videoid = 'videobg'; 
            $(function() {
                var onVideoReady = function(){
                  setTimeout(function(){
                    loadhero();
                      // playvideo(thevideo);
                   }, 0); 
                }
                var onVideoError = function(){
                    loadhero();
                    console.warn('Video failed to load');
                      // playvideo(thevideo);
                }
                var isVideoReady = new videoReady(videoid, onVideoReady, onVideoError);
                    isVideoReady.check();
            });
          // End Is Video Ready

        } else {

          // Start Is Img Ready
            var img = new Image();
            img.onload = function() {
                loadhero();
            }
            img.src = "images/x.jpg";
          // End Is Img Ready

        }
        

        function playvideo(thevideo) {
          thevideo.play();
        }

        function loadhero() {

          $('body').addClass(readyclass);
          $('header .coverup').fadeOut(1000);
          $('header .img').fadeIn(1000);
          if ($('header video').length) { 
            playvideo(thevideo);
          }
          var theease = Expo.easeOut;

          var tl = new TimelineMax();
            tl.addLabel('line')
            tl.to('header .copy .line1.hidden .line', 1, {
              top:0,
              opacity:1,
              ease: theease
            },'line+=1.3')
            tl.to('header .arrowdown', 0.2, {
              opacity:1
            })
        }

        //restart wow js on scroll to top
        // window.addEventListener('scroll', function(e) {
        // if( $(window).scrollTop() <= 200) {
        //       if ($('.wow').hasClass('animated')) {
        //         $(this).removeClass('animated');
        //         $(this).removeAttr('style');
        //         new WOW().init();
        //       }
        //       else if( $(window).scrollTop() <= 200) {
        //         //if reached bottom
        //       }
        //    }
        // });
        
        var controller = new ScrollMagic.Controller();

        $(function () { // wait for document ready        

          //http://scrollmagic.io/examples/basic/simple_tweening.html


         /***/// CSS Toggle
             // var rollin = new ScrollMagic.Scene({
             //   triggerElement: ".section-top .wrapper",
             //   triggerHook: 1
             // })
             // .setClassToggle(".section-top", "rollin")
             // .addIndicators({name: "animation5"}) 
             // .addTo(controller)



         /***/// Pin Scene
             // var pinDiv = new ScrollMagic.Scene({
             //   triggerElement: "#gallery", 
             //   triggerHook:0.14,
             //   duration: "80%"
             // })
             // .setPin("#gallery",{pushFollowers: false})
             // .addIndicators({name: "animation1"})
             // .addTo(controller);
         /***///


         /***/// TweenMax
            // var welcome__tween = TweenMax.to("section .description h3", 0.3, {scale:1.3,ease:Power2.easeInOut});
            // var scene1 = new ScrollMagic.Scene({
            //    triggerElement: ".scrolldown img",
            //    duration: "50%"
            // })
            // .setTween(welcome__tween) 
            // .addIndicators({name: "welcome"}) 
            // .addTo(controller);


         /***/// Tween Shorthand
            // var scene1 = new ScrollMagic.Scene({
            //     triggerElement: "#gallery",
            //     duration: "50%"
            // })
            // .setTween("#stripe", 0.5, {backgroundColor: "#FFD100", scale: 10}) 
            //  .addIndicators() 
            //  .addTo(controller);


         /***/// Parallax Image
             // var slideParallax = new ScrollMagic.Scene({
             //   // triggerElement: "#container2",
             //   // offset:40,
             //   duration: "110%"
             // })
             // .setTween(TweenMax.from('header .bgimg img', 0, {y: '-5%', ease:Power0.easeNone}))
             // // .addIndicators({name: "animation2"}) 
             // .addTo(controller)


             
             if( $('.amenitiespage').length ) {
               
               if (windowsize < 769) {
                     scenerise.remove(true);
                     // $('main .bgimage').removeAttr('style');
                     $('section.sidebyside1.beige .photo .img').css('top','');
               } else {
                 scenerise.addTo(controller);
               }
               $(window).resize(function(){
                 var windowsize = $( window ).width();
                 console.log(windowsize);
                   if (windowsize < 769) {
                     scenerise.remove(true);
                     $('section.sidebyside1.beige .photo .img').css('top','');
                   } else {
                     scenerise.addTo(controller);
                   }   
               });
             }


        });
      </script>
    
    </body>
  </html>