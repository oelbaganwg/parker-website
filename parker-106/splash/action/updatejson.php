<?php 
//http://localhost/html_boiler_plate/action/updatejson.php?id=headline1&page=homepage&text=Luxury%20Apartments%20for%20Rent
$page = $_POST["page"];
$id = $_POST["id"];
$text = $_POST["text"];

//get original json
$jsonString = file_get_contents('../content.json');
$data = json_decode($jsonString, true);

// update content of specific variable for page
foreach($data as $key => $value) {
  	if ($data[$key]["page"] == $page) {
  		$data[$key]['copy'][$id] = $text;
  	}
  	
}
//make new json
$newJsonString = json_encode($data);
file_put_contents('../content.json', $newJsonString);
echo "Success";