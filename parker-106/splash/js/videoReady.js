var videoReady = function(elementId, onSuccess, onError){

    this.ready = false;
    this.onSuccessCallback = typeof onSuccess === 'function' ? onSuccess : null;
    this.onErrorCallback = typeof onError === 'function' ? onError : null;
    var scope = this;

    function check(){

        var vid = document.getElementById(elementId);
        vid.load(); // load video manually (fixes hard refresh bug, when video loads after a delay)
        vid.oncanplay = function() {
            if( ! scope.ready ) {
                scope.ready = true;
                if(scope.onSuccessCallback !== null) scope.onSuccessCallback();
            }
        };
        vid.onerror = function() {
            if(!scope.ready){
              if(scope.onErrorCallback !== null) scope.onErrorCallback();
            }
        };

    }

    function isReady(){
        return scope.ready;
    }


    return {
        check : check,
        ready: isReady,
    }

}