
          //form placeholders
            $('.contactfields').on('input',function(e){
                currentinputid = "#" + $(this).attr('id');
                parentdiv = $(currentinputid).parent();
                //get the label of the parent of input
                var thelabel = parentdiv.find('label');
                countchar = $(currentinputid).val().length;
                if (countchar > 0) {
                  thelabel.fadeOut(0);
                } else {
                  thelabel.fadeIn(0);
                } 
            });

            jQuery(function($){
              $("#formdata_phone").mask("999-999-9999");
            });


            
            //wait for loading top background image
              var img = new Image();
              img.onload = function() {
                  $('body').addClass('loaded');
              }
              img.src = waitforthisimage; //var stored on each php page
              // img.src = "images/x.jpg";



            //Parallax header
            jQuery(document).ready(function($){

              var top=$(window).scrollTop();
              var windowHeight=$(window).height();

              $(window).bind('scroll', function (e) {
                  top=$(window).scrollTop();
                  // $('header').css({'background-position-y':Math.floor(top/4)});
                  // console.log(top, windowHeight);
              });
            });
              

            $(document).on('click', '#hamburger', function() {
              $('body').toggleClass("menuopen");
            });


            jQuery(function($){
               $("#formdata_phone").mask("(999) 999-9999");
            });


            
            $("form .input .holddate input.movenext").keyup(function () {
                if (this.value.length == this.maxLength) {
                  $(this).next('input').focus();
                }
            });
            //AJAX SUBMIT FORM
            // $('.formsubmit').click(function() {
            $(document).on('click', '.formsubmit', function() {

                //check full name
                if (/\w+\s+\w+/.test($("#formdata_name").val())) {
                  console.log('good');
                } else {
                  console.log('bad');
                  $('#formerror').fadeOut();
                  $('#formerror').html('Please enter a full name.');
                  $('#formerror').fadeIn();
                  return false;
                }

                // var moveindate = $('#date_month').val() + '/' + $('#date_day').val() + '/' + $('#date_year').val();
                var url = "action/contact.php";
                
                //if name is split // and account for fname var in actions/ php
                // var fname = $('#formdata_fname').val();
                // var lname = $('#formdata_lname').val();
                // var grabpostdata = $('#theform').serialize();
                // var postData = grabpostdata + "&formdata_name=" + fname + " " + lname;

                var postData = $('#theform').serialize();
                // var postData = postData_get + '&formdata_move_in_date=' + moveindate;

                $.ajax({
                  type: 'POST',
                  url: url,
                  data: postData,
                  dataType: "html",
                  beforeSend: function() {
                    // setting a timeout
                    $('.formsubmit').addClass('on');
                    $('.formsubmit').html('Sending <span>.</span><span>.</span><span>.</span>');
                    console.log(postData);
                  },
                  success: function(data) {
                     $('.formsubmit').removeClass('on');

                     if (data == "Success") {
                        $('#formerror').fadeOut();
                        $(".holdform").fadeOut(function() {
                          $(".holdthanks").fadeIn();
                        });

                        // fbq('track', 'Lead', {
                        // value: 20.00,
                        // currency: 'USD'
                        // });

                        //track Adwords conversion
                        gtag('event', 'conversion', {'send_to': 'AW-1029912699/Y4hmCK7V-KYBEPvwjOsD'});

                     } else if (data == "nameerror") {
                          $('#formerror').html('Please enter a first and last name');
                          $('.formsubmit').html('Contact Us');
                     } else {
                        $('#formerror').fadeIn();
                        $('#formerror').html(data);
                        $('.formsubmit').html('Submit');
                     }
                  }
                  // dataType: 'html'
                });
            });


            
          	//smooth scroll
            $(function() {
              // Desired offset, in pixels
              var offset = -140;
              // Desired time to scroll, in milliseconds
              var scrollTime = 850;

              $('a[href^="#"]').click(function() {
                  // Need both `html` and `body` for full browser support
                  $("html, body").animate({
                      scrollTop: $( $(this).attr("href") ).offset().top + offset 
                  }, scrollTime);

                  // Prevent the jump/flash
                  return false;
              });
            });


        //new swiper
          var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 30,
            effect:'fade',
            loop: true,
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            },
            autoplay: {
              delay: 3000,
            },
            speed:600,
            spaceBetween:0,
            navigation: {
              // nextEl: '.swiper-button-next',
              // prevEl: '.swiper-button-prev',
              nextEl: '.next',
              prevEl: '.prev',
            },
          });


        //slide to options
          // $( ".swiper-container2 .swiper-slide" ).on( "click", function() {
          //     var slider2click = swiper1.clickedIndex - 2;
          //     swiper.slideTo(slider2click, 600);
          //     $('html, body').animate({
          //             scrollTop: $('#portfolioitem').offset().top
          //           }, 800);
          // });

          // $( ".navlinks.next" ).on( "click", function() {
          //     swiper1.slideNext(600);
          // });

          // $( ".navlinks.prev" ).on( "click", function() {
          //     swiper1.slidePrev(600);
          // });

   
      //Fancybox
        $("[data-fancybox]").fancybox({
           loop     : true,
           keyboard : true,
           toolbar : true,
           animationEffect : "zoom-in-out", //false,zoom,fade, zoom-in-out
           transitionEffect : "zoom-in-out", //false,fade, slide, circular,tube, zoom-in-out, rotate
           transitionDuration : 600,
           btnTpl : {
             close : '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
                 '<svg viewBox="0 0 40 40">' +
                     '<path d="M10,10 L30,30 M30,10 L10,30" />' +
                 '</svg>' +
             '</button>',
             // Arrows
             arrowLeft : '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
                             '<svg viewBox="0 0 40 40">' +
                               '<path d="M10,20 L30,20 L10,20 L18,28 L10,20 L18,12 L10,20"></path>' +
                             '</svg>' +
                           '</button>',

             arrowRight : '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
                           '<svg viewBox="0 0 40 40">' +
                             '<path d="M30,20 L10,20 L30,20 L22,28 L30,20 L22,12 L30,20"></path>' +
                           '</svg>' +
                         '</button>'
                 },
           // What buttons should appear in the top right corner.
           buttons : [
               //'download',
               //'zoom',
               'close'
           ],
        });