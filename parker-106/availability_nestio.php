<?php include('domain.php'); ?>
<?php 

  $response = file_get_contents('https://nestiolistings.com/api/v2/listings/all/?key=ed76f328602546a88b3a2c5a92744b01');

  $properties = json_decode($response);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <title>The Parker Apartments For Rent | Studio to 2 Bedroom Rentals in NJ</title>
    <meta name="description" content="Spacious studio, 1-bedroom, and 2-bedroom rental residences at The Parker. Convenient Location Near Rutherford Train Station. On-Site Covered Parking. Premier Amenities. Now Leasing.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="css/fullpage.css"/>
    <link rel="stylesheet" href="css/fancybox.css"/>
    <link rel="stylesheet" href="css/aos.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <?php include('header-scripts.php') ?>

</head>
<body class="availability">

<!--page loader-->
<div class="page_loader"></div>
<!--end page loader-->

<!--header nav-->
<?php include('_header.php') ?>

<!-- primary-section -->
<section class="primary-section">
    <img src="images/TheParker-11-EDIT.jpg" alt="#" class="full-img">
    <div class="container">
        <h1 class="primary-title">THE CHOICE IS YOURS</h1>
        <p>Availability</p>
    </div>
    <button class="scroll-down scroll_on_screen"><i class="icon-angle-down"></i></button>
</section>

<!-- room-section -->
<section class="room-section">
    <div class="container">
        <ul class="tab-nav floorplansnav" data-aos="fade-up">
            <li class="tabs-item tabs-item-active"><a href="#"  data-id="all">ALL</a></li>
            <li class="tabs-item"><a href="#" data-id="0bed">STUDIO</a></li>
            <li class="tabs-item"><a href="#" data-id="1bed">ONE BEDROOM</a></li>
            <li class="tabs-item"><a href="#" data-id="2bed">TWO BEDROOM</a></li>
        </ul>
        <?php 

            if ($properties != "") { ?>
        <table class="table sortable table_avail" data-aos="fade-up">
            <thead>
            <tr>
                <!-- <th data-sort="float">Floor <img src="images/down-arrow.svg" alt="#"></th> -->
                <th data-sort="string">Bed/Bath <img src="images/down-arrow.svg" alt="#"></th>
                <!-- <th data-sort="float">Sq. Ft. <img src="images/down-arrow.svg" alt="#"></th> -->
                <th data-sort="float">Price <img src="images/down-arrow.svg" alt="#"></th>
                <th>Floorplan</th>
                <th ></th>
            </tr>
            <tbody>
            <?php 
              foreach ($properties->items as $property) :
                // var_dump($property);
                $ApartmentName = $property->unit_number;
                $bedrooms = $property->bedrooms;
                $bathrooms = $property->bathrooms;
                $bedslabel = NULL;
                  switch ($bedrooms) {
                      case "0":
                          $bedslabel = "Studio";
                          break;
                      case "1":
                          $bedslabel = "1 Bedroom";
                          break;
                      case "2":
                          $bedslabel = "2 Bedroom";
                          break;
                      default:
                          $bedslabel = "Studio";
                  }
                $bedbathLabel = $bedslabel . " / " . $bathrooms . " Bath";
                $sqft = $property->square_footage;
                // $ApplyNowUrl = $property->building->application_link;
                $applynowlink = "https://www.on-site.com/apply/property/275856";
                $price = number_format($property->price);
                foreach($property->photos as $photo):
                  if($photo->media_type=="Floor plan"):
                    $floorplanImage =  $photo->original;
                  endif;
                endforeach;
             
            ?>
            <tr class="<?php echo $bedrooms; ?>bed">
                <!-- <td data-label="Floor">2</td> -->
                <td data-label="Bed/Bath"><?php echo $bedrooms; ?> BED / <?php echo $bathrooms; ?> BATH</td>
                <td data-label="Sq. Ft."><?php echo $sqft; ?> sq. ft.</td>
                <td data-label="Price">$<?php echo $price; ?>*</td>
                <!-- <td class="view-floor"><a href="#modal" class="btn btn-default" data-modal="#modal">View Floorplan</a></td> -->
                <td class="view-floor"><a href="#modal" class="btn btn-default view" data-modal="#modal" 
                    data-img="<?php echo $floorplanImage; ?>" 
                    data-applynowurl="https://www.on-site.com/apply/property/275856" data-apartmentname="<?php echo $ApartmentName; ?>" data-beds="<?php echo $bedrooms; ?>"
                    data-baths="<?php echo $bathrooms; ?>" 
                    data-bedslabel="<?php echo $bedbathLabel; ?>"
                    data-sqft="<?php echo $sqft; ?> sq. ft.">View Floorplan</a></td>
                <td><a href="https://www.on-site.com/apply/property/275856" class="btn btn-primary" target="_blank">Apply</a></td>
            </tr>
            <?php 
               endforeach; 
            
             ?>

            </tbody>
        </table>
        <?php  } else {
              echo "No Current Availability.";
             } ?>
    </div>
</section>

<!-- Modal -->
<div id="modal" class="modal">
    <div class="modal-content">
        <div class="modal-left">
            <div class="modal-logo">
                <img src="images/logo-white-sm.svg" alt="#">
            </div>
            <div class="floorplan-desc">
                <!-- <h3 id="residence" class="room-title">RESIDENCE 07</h3> -->
                <div id="bedlabel">Studio / 1 Bath</div>
                <div id='sqft'>824 sq. ft.</div>
            </div>
        </div>
        <div class="modal-right">
            <div class="floorplan-img">
                <img id="floorplanimg" src="images/room.jpg">
            </div>
        </div>
        <div class="modal-btn">
            <a id="applynowurl" href="javascript:;" class="btn btn-primary" target="_blank">Apply</a>
            <a id="downloadimg" href="javascript:;" class="btn btn-primary" target="_blank">Download</a>
        </div>
    </div>
</div>

      <script>
    
        $('a.btn.view').on('click', function() {
          $(".availability #modal").attr("src","images/ajax-loader.gif");
          var beds = $( this ).data( "beds");
          var baths = $( this ).data( "baths");
          var sqft = $( this ).data( "sqft");
          var bedslabel = $( this ).data( "bedslabel");
          var applynowurl = $( this ).data( "applynowurl");
          var apartmentname = $( this ).data( "apartmentname");
          var floorplanimg = $( this ).data( "img");
          $('.availability #modal').addClass('on');
          $('.availability #modal #residence').html(apartmentname);
          $('.availability #modal #sqft').html(sqft);
          $('.availability #modal #bedlabel').html(bedslabel);
          $('.availability #modal #baths').html(baths);
          $(".availability #modal #floorplanimg").attr("src",floorplanimg);
          $(".availability #modal #downloadimg").attr("href",floorplanimg);
          $(".availability #modal #applynowurl").attr("href",applynowurl);
        });


        $('.availability #modal .closex').on('click', function() {
          $('.availability #modal').removeClass('on');
          $(".availability #modal #floorplanimg").attr("src","images/ajax-loader.gif");
        });


        $('.floorplansnav a.btn').on('click', function() {

          //scroll to table on mobile
          // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          //   $('html, body').animate({scrollTop: '+=150px'}, 500);
          // }

          //set active tab
          $('.floorplansnav a.btn').removeClass('active');
          $(this).addClass('active');
          
          //get the class wanted
          var desiredbeds = $(this).data('id');
          if (desiredbeds == "all") {
            $('table tr').removeClass('remove');
          } else {
            $('table tr').removeClass('remove');
            $('table tbody tr:not(.' + desiredbeds + ')').addClass('remove');
          }
        });
    </script>

<?php include('_footer.php')?>