"use strict";
var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var sass = require("gulp-sass")(require("sass"));
var postcss = require("gulp-postcss");
var concat = require("gulp-concat");
var terser = require("gulp-terser");
var replace = require("gulp-replace");
var cacheBustingTask = "Bust cache";
var csstasknamechange = "Updated CSS";
var jstasknamechange = "Updated JS";
var tasknamewatch = "watch";
var maincss_srcfile = "scss/styles.scss";
var stylesoutputfolder = "styles";
var watchcss = "scss/**/*.scss";
var watchjs = "js/**/*.js";

//Cache bust
gulp.task(cacheBustingTask, function () {
  var cbString = new Date().getTime();

  gulp
    .src(["header-scripts.php", "footer-scripts.php"])
    .pipe(replace(/v=\d+/g, `v=${cbString}`))
    .pipe(gulp.dest("."));
});

//CSS compile
gulp.task(csstasknamechange, function () {
  // gulp.src('scss/*.scss') //write multiple files
  gulp
    .src([maincss_srcfile], {
      sourcemaps: true,
    }) //write one file
    .pipe(concat("styles.min.css"))
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    // minify
    .pipe(gulp.dest(stylesoutputfolder, { sourcemaps: "." }))
    .pipe(browserSync.stream());
});

//JS Compile
gulp.task(jstasknamechange, function () {
  gulp
    .src(["./js/main.js"], {
      sourcemaps: true,
    })
    .pipe(concat("all.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./scripts/", { sourcemaps: "." }))
    .pipe(browserSync.stream());
});

//Watch Tasks
gulp.task(tasknamewatch, function () {
  var files = ["*.php"];
  browserSync.init(files, {
    proxy: "localhost/parker-website/parker-106", // change subdirectory to match website name but should load as a subdirectory inside localhost.
    // ui: {
    //   port: 3001
    //   },
    // port: 3000,
  });
  gulp.watch(watchcss).on("change", gulp.series(csstasknamechange));
  gulp.watch(watchjs).on("change", gulp.series(jstasknamechange));
  // gulp.watch(watchcss).on("change", gulp.series(cacheBustingTask));
  // gulp.watch(watchjs).on("change", gulp.series(cacheBustingTask));
});

// Installation
// 1. Open terminal inside directory
// 2. enter command: npm install
// 3. enter command: gulp watch
