<?php
   header("Location: https://renttheparker.com/parker-118/");
   exit();
   ?>

<?php include('domain.php'); ?>
<!DOCTYPE html>
<html>

<head>
  <title>The Parker | Discover Park Avenue Living | Rutherford, NJ</title>
  <meta name="description" content="The Parker 118 is now leasing new studio, one, and two bedroom luxury rentals with amenities on Park Avenue. Also connect with The Parker 106.">
  <?php include('header-scripts.php'); ?>
</head>

<body>
  <main>
    <section class="home-hero">
      <div class="container copy-container">
        <div class="logo-container">
          <img src="images/icons/Parker118_logo-white.svg" alt="">
        </div>
        <p>New Studio, One & Two Bedroom Luxury Rentals<br>Now Leasing</p>
        <a href="./parker-118">
          <button class="discover-btn">Schedule your visit</button>
        </a>
      </div>

      <div class="swiper-background-img-container">
        <!-- Core Swiper Without Styles -->
        <!-- Slider main container -->
        <div class="swiper-home">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide">
              <img src="images/Parker_1.jpg" alt="">
            </div>
            <div class="swiper-slide">
              <img src="images/Parker_2.jpg" alt="">
            </div>
            <div class="swiper-slide">
              <img src="images/Parker_3.jpg" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="lower-banner">
        <h2>Interested in The Parker 106? <a href="./parker-106">Connect With Us Here</a></h2>
      </div>
    </section>


    <footer>
      <div class="container footer-flex">
        <address class="footer-child">
          <p>The Parker 118 <span>/</span></p>
          <p>118 Park Avenue, Rutherford, NJ 07070 <span>/</span></p>
          <p>
            <a href="tel:201-933-2106">201.933.2106 <span class="show">/</span>
              <a href="mailto:info@renttheparker.com">Info@RentTheParker.com</a>
          </p>
        </address>

        <address class="footer-child">
          <p>The Parker 106<span>/</span></p>
          <p>106 Park Avenue, Rutherford, NJ 07070 <span>/</span></p>
          <p>
            <a href="tel:201-933-2106">201.933.2106 <span class="show">/</span>
              <a href="mailto:info@renttheparker.com">Info@RentTheParker.com</a>
          </p>
        </address>

        <div class="footer-child footer-logos">
          <img src="images/icons/Vango-logo-white.svg" alt="">
          <img src="images/icons/EHO-logo-white.svg" alt="">
          <img src="images/icons/ADA-logo-white.svg" alt="">
        </div>
      </div>
    </footer>


  </main>


  <?php include('footer-scripts.php'); ?>
  <script>
    //Basic Swiper Initialization
    const swiperHome = new Swiper(".swiper-home", {
      // Optional parameters
      direction: "horizontal",
      loop: true,
      speed: 1000,
      effect: "fade",
      autoplay: {
        delay: 5000,
      },

      // If we need pagination
      // pagination: {
      //   el: '.swiper-pagination',
      // },

      // Navigation arrows
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },

      // And if we need scrollbar
      // scrollbar: {
      //   el: '.swiper-scrollbar',
      // },
    });
  </script>

  <script>
    var tl = gsap.timeline();
    tl.to(".home-hero", {
      autoAlpha: 1,
      duration: 1
    });
    tl.to(".copy-container", {
      autoAlpha: 1,
      y: -50,
      duration: 1.5
    });
    tl.to(".lower-banner", {
      autoAlpha: 1,
      duration: 1
    }, 1);
  </script>
</body>

</html>